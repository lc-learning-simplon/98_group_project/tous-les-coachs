# SIMPLON-Readme

***
## QUIK-START
1. Avoir installé Composer en global.
2. Clonez le projet.
3. `composer install`
4. Créer un `.env` à la racine du projet et y intégré le contenu du `env_exemple`
5. Configurer le `.env` :
`ENVIRONNEMENT-CLI` => simplon / development / production.
`app.baseUrl='http://localhost:8080'` => simplon / development.
`database.default.*`=> Vos indentifiants Mysql.
`API-KEY-MAPQUEST`  => Votre [Clé API](https://developer.mapquest.com/documentation/geocoding-api/).
`HASH` => Votre clé de hash.
6. Créer votre base de donnée.
7. `php spark migrate` => Crée les tables.
8. `php spark db:seed Roles`
9. `php spark db:seed Categories`
10. `php spark db:seed Users` => **A supprimer en production**
11. `php spark serve`

***

## En avant vers l'aventure
> ### Petit rappel sur le modèle MVC

=> MVC abbréviation pour Model View Controller une mèthode visant à améliorer l'organisation de son code de manière à séparer les données, l'affichage et la logique d'une application en 3 parties bien distinctes.
=> Dans notre cas, cela se traduit bien par les sous-dossiers suivants dans le dossier 'app':
 - un dossier 'Controllers' (responsable de la logique)
 - un dossier 'Views' (en charge de l'affichage)
 - un dossier 'Database', un dossier 'Entities' et un dossier 'Models' (en charge des données)
**[A noter]** Les routes sont définies dans le dossier app/Config/Routes.php et se présentent sous la forme suivante:
`$routes->HTTP_method('url_appelée', 'Controller::class_method');`
Exemple :
 `$routes->get('/', 'Home::index');`



> ### L'affichage avec le système de 'views'

Tout ce qui a trait à l'affichage est regroupé, par convention, dans le dossier 'Views'. Il est souhaitable de créer des sous-dossiers et d'y organiser les différents fichiers relevant de la même thèmatique. Ainsi, si notre application posséde une section coach et une autre section livre subdivisées elles memes en plusieurs pages il serait intéressant de considérer la création d'un dossier 'coach' et d'un autre dossier 'livre' dans le dossier 'views'. Il est également assez commun de trouver un dossier 'templates' dans lequel on retrouve un fichier 'header.php', un fichier 'footer.php' ainsi que parfois un fichier 'menu.php'.
Cette organisation se reflète dans le controller avec la succession des 'echo view()'pour afficher la page.
```
class Home extends BaseController
{
	public function index()
	{  
		echo view('templates/themeHeader');
        echo view('pages/home');
        echo view('templates/themeFooter');
	}

}
```
Cela permet de n'avoir qu'un seul header et qu'un seul footer centralisé en un même lieu et commun à toutes nos pages. Ainsi, si jamais l'envie nous prend de les modifier, il nous suffira d'intervenir sur ces 2 fichiers et l'ensemble des pages sur lesquelles ils sont inclus reflétera automatiquement ces changements. De la meme manière, cela permet aux pages de ne contenir que le contenu qui leur est propre. 

> ### Migrations et modéles

1. **Les migrations**
Ce sont des fichiers, exécutables depuis le terminal avec la commande `php spark migrate:refresh`, qui permettent le partage de la structure d'une base de donnée. Mais, avant de les éxécuter, il va falloir les créer.
Ceci se fait via la commande (toujours depuis le terminal) `php spark migrate:create nomDeLaTableQueLonVeutCréer `. (Ceci implique d'avoir créer une base de donnée auparavant.)
Cette commande a pour conséquence la création d'un fichier nommé sous le format 'datedujour-nombre_'nomDeLaTableQueLonVeutCréer.php' dans le dossier app/Database/Migrations. Le fichier posséder déjà une architecture basique dans laquelle on va pouvoir placer nos instructions.
```
<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class nomDeLaTableQueLonVeutCréer extends Migration
{
  public function up(){}
  public function down(){}
};
```
Exemple:
 Dans notre code, une migration a été créée pour créer la table UserRole...
```<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserRole extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();
		$this->forge->addField([
			'user_role_id'		=> [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'user_id'		=> [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE
			],
			'role_id'		=> [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE
			]
		]);
		$this->forge->addForeignKey('user_id', 'users', 'user_id');
		$this->forge->addForeignKey('role_id', 'roles', 'role_id');
		$this->forge->addKey('user_role_id', TRUE);
		$this->forge->createTable('user_role');
		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('user_role');
	}
}
```
 ligne 10.  ce code est utile dés lors que la table concernée va contenir des clés étrangéres.
 ligne 11. on demande à forge, l'outil dédié de CodeIgniter, de nous rajouter les champs que l'on va définir par la suite
 (dans notre exemple 'user_role_id', 'user_id', 'role_id').
 Pour chacun d'entre eux, on va ensuite définir des charactéristiques. (Je veux que le champs 'user_role_id' soit obligatoirement un nombre entier (INT) qu'il ne dépasse pas une série de 5 chiffres, qu'il soit obligatoirement positif et que sa valeur augmente automatiquement de 1 à chaque nouvelle création). 
 ligne 29. On signale à forge que le champs 'user_id' dépend de la table 'users' et de son propre champs 'user_id'
 ligne 31. On signale à forge que l'on veut faire de notre champs 'user_role_id' notre clé primaire pour cette table.
 ligne 32. On crée la table
 ligne 40. Cette commande permet de supprimer la table 
 
 La migration créée est maintenant disponbile pour tous via la commande `php spark migrate:refresh`.
Lien vers la documentation de CI: [Database Migrations — CodeIgniter4 4.0.0-rc.3 documentation](https://codeigniter4.github.io/userguide/dbmgmt/migration.html)

Le principe est assez similaire pour la création de 'seed' autrement dit pour la création de données et leur injection dans la base.
Lien vers la documentation de CI:[Database Seeding — CodeIgniter4 4.0.0-rc.3 documentation](https://codeigniter4.github.io/userguide/dbmgmt/seeds.html)

2. **Les modèles** 

On peut les trouver dans le dossier app/Models et leur création est détaillée dans la documentation de CI
[Using CodeIgniter’s Model — CodeIgniter4 4.0.0-rc.3 documentation](https://codeigniter4.github.io/userguide/models/model.html) dans la rubrique 'Configuring your model'.
La création de ces 'models' permet d'accèder à un certains nombre de méthodes prédéfinies que l'on trouve bien utiles lorsque l'on doit faire des requetes sur la base de données.
En effet, ceci nous évite de faire des requetes en dur et d'avoir à se connecter à la base de données à chaque fois que l'on a besoin d'y avoir accés.

 `\Config\Database::connect();`
  `$query = $db->query('requete_sqp_en_dur')`
  `$results = $query->getResult();`

Les mèthodes disponibles suite à la création d'un model, à son appel en haut de page (avec un `use App\Models\nomDuModelAvecOnVeutAvoirAcces;
`) et à son instanciation ( mettre dans la méthode du controller `$NomAléatoire = new NomDuModelAppelé;`
) sont listées dans la documentation dans la rubrique 'working with data' ( find(), findAll(),save(), delete()...)
[Using CodeIgniter’s Model — CodeIgniter4 4.0.0-rc.3 documentation](https://codeigniter4.github.io/userguide/models/model.html#working-with-data)

> ### Les 'filtres' ou comment protéger les routes
Les filtres peuvent être créés dans le dossier 'app/Filters'.
A l'intérieur, il s'agit de définir une logique dans les 'functions before() et/ou after()' afin d'entourer le controller éxécuté par une route de certaines conditions. Dans ce sens, ils permettent également d'économiser des lignes de code car un même filtre peut etre appliqué à plusieurs routes. (Plusieurs filtres peuvent aussi etre assignés à une même route.)

`public function before(){` la logique du controller ne s'éxécutera que si les conditions stipulées à l'intérieur de ce bloc sont remplies `};`
`public function after(){` la logique définie dans cet interval s'appliquera systèmatiquement après l'éxécution de celle du controller `};`
**Exemple d'utilisation :** On souhaite protéger un ensemble de route en ne permettant qu'aux utilisateurs possédant un certain role d'y accéder... On crééra donc un filtre, placera notre logique dans la fonction before() et l'appliquera à la route de cette manière.
*Pour plus d'information* -> [Controller Filters — CodeIgniter4 4.0.0-rc.3 documentation](https://codeigniter4.github.io/userguide/incoming/filters.html?highlight=filter)

> ### Faire voyager l'information sur plusieurs pages
1. **La session sur CodeIgniter**
Si on veut travailler avec cet objet dans notre controller, ne pas oublier d'inclure cette ligne avant la définition de la classe:
`$session = \Config\Services::session();`
Ceci fait, la session devra aussi etre instanciée dans chaque méthode du controller souhaitant utiliser la session avec un :
`$session = session();`
L'objet 'session' devient donc disponible ainsi que les différentes méthodes qui lui sont associées...
Il est très pratique pour le développeur car il permet de conserver certaines informations comme l'id utilisateur au moment de la connexion de ce dernier.


2. **Les messages flash**
Bien que similaire dans leur comportement, les messages flash sont à préférer dans le cas où l'information à communiquer n'a pas vocation à être conservée tout au long de la navigation de l'utilisateur sur le site.
Dans ce cas-ci, plutot que la session, utiliser cet autre mécanisme pour afficher une information:
`return redirect()->back()->with('success',"Bienvenue $checkUser[first_name] $checkUser[last_name] ");`
Ici, l'utilisateur est redirigé vers une autre page (celle où il se trouvait) avec (`->with`) le message`'success'` correspondant à `Bienvenue...`
Autre exemple:
`return redirect()->back()->with('error','mot de passe incorrect');`

> ### MapQuest API
Cette API a été utilisée afin de convertir toutes les adresses fournies par les coachs en des coordonnées corrrespondant à la latitude et à la longitude d'un lieu.
[Open Geocoding API - Overview \| MapQuest API Documentation](https://developer.mapquest.com/documentation/open/geocoding-api/)

>### Openstreetmap  leaflet 



## Le coin du Développeur
> ### Les outils
+ MySQL Workbench (similaire à PHPMyAdmin) **[Pratique]** Ce logiciel dispose d'un onglet 'Database' où est logé la fonctionnalité 'reverse engineer' qui permet, en un click, de transformer une base de donnée en un diagramme qui permet d'avoir un meilleur visuel sur l'ensemble des relations de la base.
+ Lucidchart Lien: [Logiciel de création de diagrammes et de supports visuels \| Lucidchart](https://www.lucidchart.com/pages/fr)
+ Trello (pour rester organisé et faciliter le travail d'équipe) Lien: [Trello](https://trello.com/)  
+ Taiga.io (pour rester organisé et faciliter le travail d'équipe) Lien: [Taiga.io](https://taiga.io/)




> ## Points de réflexion

1. **Le bouton de connexion**

Etant donné la finalité de la plateforme (attirer des coachs et faire qu'ils s'inscrivent sur la plateforme), ce bouton pourrait être un peu plus visible et spécifiquement s'adresser à eux. Il pourrait ainsi être renommé avec un intitulé comme "Devenir coach sur cette plateforme".
Cliquer dessus déclencherait l'ouverture d'une fenêtre modale
explicitant, en quelques lignes, l'intérêt pour eux à être référencé sur la plateforme. (ex: Augmentez votre visibilité, boostez votre chiffre d'affaire, simplifiez vos échanges avec votre clientèle... et tout cela GRATUITEMENT).
Un petit formulaire d'inscription suivrait :
+ champs nom
+ champs prénom
+ champs mail
+ champs adresse ( en justifiant la collecte de cette information/ ex: pour obtenir une visibilité immédiate auprès de nos utilisateurs/ notre réseau d'utilisateurs )

2. **Vitrine des coachs**

Faire figurer les mails et numéros de téléphone directement sur les pages non soumises à authentification expose leurs propriétaires à un risque de spam non négligeable. De la même manière, il est possible que, légalement, la publication de telles données (sensibles et personnelles) doive faire l'objet d'une demande d'autorisation préalable auprès des inscrits. (de type: Acceptez-vous que ces données figurent sur notre site web et soient disponibles pour l'ensemble des internautes ?).