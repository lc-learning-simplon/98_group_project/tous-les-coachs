// dev-commun.js
// version de développement
// ce fichier contient des fonctions qui servent pour debugger.
// Elle ne sont utiles que lors de la phase de développement.


//###############################################################
// Divers
//###############################################################
    //=========================================================================
    // Pour le chargement dynamique de scripts
    //=========================================================================
        // fonction qui permet de charger un script javascript  
        /*               
        function includeScript(fileName){
            // NB: attendre que le dom soit chargé
            // mais si le DOM est complètement chargé, est ce que le script va être exécuté ?
            document.write("<script type='text/javascript' src='"+fileName+"'></script>" );
        } // function includeScript(fileName)
        */

//###############################################################
// variables globales
//###############################################################
        
    //===========================================================
    // ressources
    //===========================================================

        /*---------------------------------------------------------------------------------------
            ces variables avec les chemins et noms de fichiers permet de s'adapter plus
            facilement si on doit changer l'arborescence des fichiers
        ----------------------------------------------------------------------------------------*/

        // variable pour les appels ajax
        //==============================
            var ajaxCall = {};

            // racine commune à toutes les URL pour ajax
            //ajaxCall.root = "?ajax=";
            ajaxCall.root = "http://TLC3/ajax/";

            // utilisé par commun.js
            ajaxCall.statClic       = "statClic"; // pour enregistrer des infos sur les clics utilisateur 
            ajaxCall.statUser       = "statUser"; // pour enregistrer des infos sur utilisateur dans BDD

            // utilisé par homeCoachs.js
            ajaxCall.statPos        = "statPos"; // pour enregistrer des infos sur la position dans BDD
            ajaxCall.coachsProxi    = "coachsProxi"; // demande la liste des coachs à proximité
            ajaxCall.coachSearch    = "coachSearch"; // recherche un coach par son nom

        // variable pour les chemins
        //==========================
            var chemin = {};

            // NB : les chemins ci-dessous sont donnés à partir du répertoire racine du projet.
            chemin.icones           = "public/icons/";  // chemin du répertoire des icônes
            chemin.images           = "public/images/"; // chemin du répertoire des images
            chemin.json             = "public/json/";   // chemin vers les fichiers.json

        
        // variables pour les noms de fichiers
        //====================================
            var nomFic = {};
             
             // objets pour les différents types de fichiers
             nomFic.php  = {}; 
             nomFic.svg  = {};
             nomFic.png  = {};
             nomFic.jpg  = {};
             nomFic.json = {};
         
         // nom de fichiers génériques (utilisés par toutes les pages)
         //===========================================================
         
        // noms de fichier utilisés par home-coachs.js
        //============================================
            // fichiers .svg
            //--------------
            nomFic.svg.markerUserPos            = "Marker-bleu.svg";                // marker pour la position de l'utilisateur
            nomFic.svg.boutonLocCoach           = "icon-adresse3-marker-blanc.svg"; // bouton permettant de localiser le coach sur la carte
            nomFic.svg.boutonLivresConseilles   = "Livre-blanc.svg";                // bouton pour voir les livres conseillés par le coach
            nomFic.svg.iconeTitreCoach          = "titre-bleu.svg";                 // icône permettant d'introduire le titre du coach
            nomFic.svg.iconeAdresse             = "marker-bleu-2.svg";              // icône permettant d'indiquer que ce qui suit est une adresse.
            nomFic.svg.iconeTel                 = "tel-bleu.svg";                   // icône permttant d'indiquer que ce qui suit est un n° de tel
            nomFic.svg.iconePageWeb             = "globe-bleu-2.svg";               // icône permettant d'indiquer que ce qui suit est une page web.
            
            // fichiers .jpg
            //--------------
            nomFic.jpg.prefixePhotoEncart       = "coach-";                         // il s'agit d'un préfixe pour les photos des encarts Coachs
            nomFic.jpg.suffixePhotoEncart       = "-photo-encart.jpg";              // il s'agit d'un suffixe pour les photos des encarts coachs.
            
            // fichiers .json
            //---------------
            nomFic.json.simuRechercheCoachPHP   = 'simu-rechercheCoachPHP.json';    // ce fichier simule une réponse du serveur par le script rechercheCoach.php

    //===============================================================
    // variable pour le fonctionnement
    //===============================================================
        var fonctionnement = {};
        
        // Pour vérifier la fin de l'initialisation.
        // Elle sera mise à jour à la fin de initialisation.
        fonctionnement.initOK = false;

        // durée des timeout pour les API et échanges ajax, avant de
        // conclure que ça ne fonctionne pas
        fonctionnement.dureeTimeout = 1500; // (ms)

    //===============================================================
    // variable avec les infos d'environnement
    //===============================================================
        var env = {};
        
        // taille écran
        env.ecran = {};
        env.ecran.largeur = null;
        env.ecran.hauteur = null;
        
        // limites de tailles d'écran
        env.ecran.xs = 0;
        env.ecran.sm = 576;
        env.ecran.md = 768;
        env.ecran.lg = 992;
        env.ecran.xl = 1200;
        
    //===============================================================
    // variable avec les catégories de coaching
    //===============================================================
    var tlc = {};

    // définition des catégories
    tlc.categories =  ["Développement personnel", "Santé", "Sport", "Business", "Psychologie", "cat5", "cat6", "cat7", "cat8", "cat9"];

//###############################################################
// Gestion des logs console
//###############################################################
    //=========================================================================
    // infoDebug()
    //=========================================================================
        function infoDebug(cat, msg){
            /*
                Cette fonction affiche des messages dans la console, mais uniquement si la
                variable globale option.debug est à true, si la variable option.debugcat.categorie est à true.
                C'est une fonction qui ne sert qu'en environnement de développement.

                Entrées :
                ---------
                cat : catégorie de message, si cat=="", alors, le message est considéré comme ne faisant pas
                      partie d'une catégorie donnée.
                msg : mesage à afficher dans la console

                Sorties : 
                --------- 
                Sortie sur console.debug()
            */

            if((typeof(option) != "undefined") && option.debug){

                // préparation des couleurs associées à chaque catégorie
                var couleurs = ['dodgerblue', 'fuchsia', 'teal', 'gold', 'coral', 'turquoise', 'orangered', 'yellowgreen']; // liste des couleurs
                var couleurCat = []; // init du tableau
                var index = 0; // init de l'index
                option.debugCat.forEach(function (item, key){  // pour chaque catégorie
                    couleurCat.push([key, couleurs[index]]); // un élément du tableau avec la catégorie et la couleur correspondante
                    index++;  // MAJ du compteur
                });
                couleurCat = new Map(couleurCat); // on transforme ça en map


                // gestion de l'affichage du message
                if(cat == ""){
                    // si pas de catégorie, on l'affiche sans gestion de couleur
                    console.debug(msg);
                } else if(option.debugCat.get(cat) != "undefined"){
                    // si la catégorie est connue
                    if(option.debugCat.get(cat)) {
                        // si l'option d'affichage de la catégorie est "true"
                        // => on l'affiche avec gestion de la couleur
                        console.debug("%c"+msg, "color:"+couleurCat.get(cat));
                    }
                } else {
                    // Sinon, catégorie inconnue
                    // => on affiche un warning
                    console.warn("dev-commun.js : infoDebug() : catégorie inconnue ("+cat+")");
                    console.warn("       => message associé = "+msg);
                }
            } // if(option.debug)
        } // function infoDebug()

    //==========================================================================
    // errorDebug()
    //==========================================================================
        function errorDebug(msg){
            /*
                Cette fonction affiche une message d'erreur dans la console, mais uniquement si la
                variable globale option.debug est à true.

                Entrées :
                ---------
                msg : mesage à afficher dans la console

                Sorties : 
                --------- 
                Sortie sur console.error()
            */

            if((typeof(option) != "undefined") && option.debug){
                // affichage du message
                //console.error(msg);
                console.debug("%c"+msg, "background-color: red");
            } // if(option.debug)
        } // function errorDebug()

    //==========================================================================
    // warnDebug()
    //==========================================================================
        function warnDebug(msg){
            /*
                Cette fonction affiche un message de warning dans la console, mais uniquement si la
                variable globale option.debug est à true.

                Entrées :
                ---------
                msg : mesage à afficher dans la console

                Sorties : 
                --------- 
                Sortie sur console.warn()
            */

            if((typeof(option) != "undefined") && option.debug){
                // affichage du message
                //console.warn(msg);
                console.debug("%c"+msg, "background-color: yellow");
            } // if(option.debug)
        } // function warnDebug()

    //==========================================================================
    // importantDebug()
    //==========================================================================
        function importantDebug(msg){
            /*
                Cette fonction affiche une message d'erreur dans la console, mais uniquement si la
                variable globale option.debug est à true.

                Entrées :
                ---------
                msg : mesage à afficher dans la console

                Sorties : 
                --------- 
                Sortie sur console.error()
            */

            if((typeof(option) != "undefined") && option.debug){
                // affichage du message
                console.debug("%c"+msg, "background-color:#ffdc36");
            } // if(option.debug)
        } // function importantDebug()

//###############################################################
// Debug
//###############################################################
    function affInfoDebug(txtDebug){
        /*-------------------------------------------------------
            function affInfoDebug()

            Cette fonction affiche de l'info, directement sur
            la page web, dans l'élément #TLC-aff-infos-debug

            Entrées : 
            ---------
            txtDebug : chaîne de caractères à afficher
        -------------------------------------------------------*/

        // on affiche le texte de débug uniquement si option.debug = true
        if((typeof(option) != "undefined") && option.debug){
            // partie en jQuery
            $(function () {
                $("#TLC-aff-infos-debug").append("<p>"+txtDebug+"</p>");
            }); // partie en jQuery
        } // on affiche le texte de débug uniquement si option.debug = true
    } // function affInfoDebug()

//###############################################################
// AJAX
//###############################################################

    //==========================================================================
    // Pour debugger ajax
    //==========================================================================
    function ajaxDebugOn(){
        /*---------------------------------------------------------------------------------
        ajaxDebugOn()

        Cette fonction permet d'afficher un diagnostique dans la console en cas d'erreur
        de transmission ajax

        Dépendances : 
            - jQuery
        ---------------------------------------------------------------------------------*/

        // debug
        infoDebug("log", "ajaxDebugOn()");
        
        $(function(){ // partie en jQuery
          // définition d'une fonction à appeler en cas d'erreur ajax. Si c'est le cas, elle affichera un 
          // diagnostique dans la console (en mode DEV).
          $(document).ajaxError(function(e, xhr, opt, errorMsg){
            errorDebug("ajaxError() : Error requesting " + opt.url + ": status = " + xhr.status + " ; statusText = " + xhr.statusText);
            errorDebug("errorMsg = " + errorMsg + " ; xhr.responseText = " + xhr.responseText);
            //errorDebug("xhr.getResponseHeader() = " + xhr.getResponseHeader());
            errorDebug("xhr.readyState = " + xhr.readyState);
            errorDebug("xhr.getAllResponseHeaders() = ", xhr.getAllResponseHeaders());
            console.log(opt);
            console.log(xhr);
            errorDebug("NB: les objets opt et hxr sont dispo dans la console, dans le filtre 'journaux'");
          });
        }); // partie en jQuery
    } // function ajaxDebugOn()
    
    //==========================================================================
    // debug : affichage des données reçues par ajax
    //==========================================================================
    function testDonneesRecues(donneesStr){
        /*---------------------------------------------------------------------------------
            testDonneesRecues()

            Cette fonction affiche les données reçues du serveur par ajax.
            Elle permet notamment de tester des échanges ajax.
            Il suffit de la mettre comme callback pour la reception des données, lors
            de l'envoi des données au serveur.
        ---------------------------------------------------------------------------------*/

        // debug
        infoDebug("log","testDonneesRecues()");
        infoDebug("chaineJson", "testDonneesRecues() : chaine JSON, méthode POST : "+donneesStr);
        infoDebug("test", "testDonneesRecues() : chaine JSON, méthode POST : "+donneesStr);
    } // function testDataRecues()

    //==========================================================================
    // Pour envoyer des infos au serveur
    //==========================================================================
    function sendToServer(ajaxRoot, ajaxFunc, data, callbackFunc){
        /*---------------------------------------------------------------------------------
            sendToServer()
            
            Cette fonction transforme les données d'entrée en une chaîne json et l'envoie
            au serveur par ajax. il est possible de donner une callback à appeler lors de la reception
            du message retour du serveur.

            Entrées : 
            ---------
                ajaxRoot    : racine pour dire au rooteur d'appeler du ajax
                ajaxFunc    : nom de la fonction ajax à appeler sur le serveur
                               NB: si option.serverOff = true et qu'une callback est fournie pour
                               traiter la reception des données, alors on utilise un fichier .json pour
                               simuler la réponse du serveur. le nom de ce fichier .json est construit comme suit : 
                                        ajaxFunc + ".json"
                data         : données à envoyer. pas besoin de les mettre en chaîne
                callbackFunc : Ce paramètre est optionnel.
                              Il s'agit de la callback à appeler une fois la réception du message retour
                              envoyé par le serveur.
                              Si la callback n'est pas définie, aucune fonction n'est appelée lors
                              de la reception du message retour envoyé par le serveur.
            
            Sorties : 
            ---------
            
        ---------------------------------------------------------------------------------*/
    
        // debug
        infoDebug("log", "sendToServer()");
        
        // si mode de test, sans serveur
        if((typeof(option) != "undefined") && option.serverOff){ 
            if(typeof(callbackFunc) != "undefined"){ // si une callback est définie
                // alors, comme on est en mode sans serveur, on va chercher les infos dans
                // le fichier .json correspondant.
                    
                // debug
                importantDebug("sendToServer() : mode option.serverOff : les données sont extraites du fichier .json plutôt que demandées au serveur");
                
                // requête pour récupérer l'objet json depuis un fichier
                $.getJSON(chemin.json + ajaxFunc+".json", function(receivedData){
                    // attention : avec le $.getJSON, on reçoit directement un objet javascript et non une chaîne json, comme pourrait le renvoyer 
                    // le serveur. Il faut donc transformer ça en une chaîne json avant de l'envoyer à receptionCoachsProches
                    var donneesStr = JSON.stringify(receivedData); // crée une chaîne json à partir des données

                    // debug
                    infoDebug("chaineJson", "chaîne json reçue du fichier .json = " + donneesStr);

                    callbackFunc(donneesStr);    // appel au script de réception de la chaîne json
                    })
                 .fail(function(){
                    errorDebug("sendToServer() : $.getJSON() : échec");
                 }); 
            } // si une callback est définie
        
        } else { // mode avec serveur
            // envoi des infos au serveur
            // par méthode post (plus de sécurité car l'utilisateur n'a pas accès à ce qui est envoyé)

            // debug
            infoDebug("chaineJson", "chaîne envoyée par sendToServer() :");
            infoDebug("chaineJson", JSON.stringify(data));
            infoDebug("info", "sendToServeur : envoi à l'adresse : " + ajaxRoot + ajaxFunc);

            if(typeof(callbackFunc) != "undefined"){ // si une callback est définie
                $.post(ajaxRoot + ajaxFunc, data, callbackFunc)
                .fail(function(){
                    errorDebug("sendToServer() : $.post(), avec callback => échec ");
                 }); ; // $.post  

            } else { // si la callback n'est pas définie
                $.post(ajaxRoot + ajaxFunc, data)
                .fail(function(){
                    errorDebug("sendToServer() : $.post() => échec");
                 }); ; // $.post  
            } // si une callback est définie - ou non


        } // mode sans ou avec serveur
    } // function sendToServer()
    
//###############################################################
// Statistiques
//###############################################################

    //===========================================================
    // doc sur l'utilisation des statistiques
    //===========================================================
        /*
        doc sur l'utilisation des statistiques
        ======================================
            
        Pour le fonctionnement des statistiques, il est nécessaire : 
        - d'appeler initStats(nomPage, versionPage); lors de l'initialisation de la page

        statistiques sur les clics utilisateurs
        ---------------------------------------
        Il est nécessaire : 
        - d'appeler majStatClic(); à l'a fin de l'initialisation d'une page, et à chaque fois
          qu'on ajoute des éléments dynamiquement sur la page. Ca permet notamment de repérer
          tous les éléments sur lesquels ajouter un évènement pour appeler statClic().
        - de fixer une priorité (variable globale stats.priorité dans dev-commun.js) en deça 
          de laquelle les évènements de stats sont envoyées à la base de données. 
          sinon, par défaut la priorité max est 5.
          NB: pour le moment, stats.priorite = tlcParam.stats.priorite; ce qui permet de
          reporter le choix de la priorité max dans dev-parametres.js.
          
        Pour ajouter des éléments "statistiquables", 
        c'est à dire, pour lesquels on veut enregistrer des statistiques :
        
        - obligatoire : 
            - classe .statClic pour chaque élément pour chaque élément à suivre
            - attribut [sc-nom], avec un nom pour l'élément. C'est ce nom qui sera enregistré dans la base de données.

        - optionnel : 
            - attribut [sc-param1] : cet attribut sera enregistré dans la base de données
            - attribut [sc-param2] : cet attribut sera enregistré dans la base de données.
            - attribut [sc-lienSortie] : On ajoute cet attribut, avec une chaîne vide, ou "true", pour signaler qu'il s'agit 
                                      d'un lien permettant de sortie de la page
            - attribut [sc-priorite] : permet de définir un nombre compris entre 1 et 5 et définissant la priorité 
                                    d'enregistrement des stats pour cet élément. 1 = priorité la plus haute. Par défaut, priorite="5";
            - attribut [sc-typeParam1] : {"text", "val"} permet de fixer sc-param1 = le text ou la valeur de l'élémént à suivre.
                                      Attention : si sc-param1 et sc-typeParam1 sont définis, c'est sc-typeParam1 qui prend le dessus.
            - attribut [sc-typeParam2] : {"text", "val"} permet de fixer sc-param2 = le text ou la valeur de l'élémént à suivre.
                                      Attention : si vparam2 et sc-typeParam2 sont définis, c'est typeParam2 qui prend le dessus.
                                      
        exemples : 
        <a class="statClic" sc-nom="bouton-choix" href="#" >Livres</a>
        <a class="statClic" sc-nom="bouton-choix" sc-typeParam1="text"  sc-lienSortie="" href="#" >Livres</a>
        <a class="statClic" sc-nom="bouton-choix" sc-param1="bouton-important"  sc-lienSortie="" sc- href="#" >Livres</a>
        <a class="statClic" sc-nom="bouton-choix" sc-Param1="bouton" sc-param2="livres" sc-lienSortie="" sc-priorite="2" href="#" >Livres</a>
        
        
        Infos supplémentaires : 
        pour connaître la liste des éléments suivis pour les stats, il suffit de taper dans la console : 
                stats.listeElements
        => il s'affiche alors un array avec tous les éléments suivis.
        Attention : ça ne fonctionne que si option.debug = true;   (voir le script param.js)
        
        statistiques sur l'utilisateur
        ------------------------------
        ce sont des statistiques anonymisées.
        Elles sont lancées automatiquement par initStats()



        */
    
    //===========================================================
    // variables globales pour les statistiques
    //===========================================================

        // variable pour les statistiques.
        var stats = {};
        
        // priorité en deça de laquelle les stats sont enregistrées
        //NB: tlcParam.stats.priorite est une variable globale définie dans dev-commun.js
        if(typeof(tlcParam) != "undefined"){
            stats.priorite = tlcParam.stats.priorite;
        }
        else{
            stats.priorite = 5;
        }

        // nom de la page
        stats.nomPage = null;
        
        // version de la page
        stats.versionPage = null;
        
        // referrer
        stats.referrer = null;
        
        // type de navigateur
        stats.nav = "inconnu";
        
        // taille écran
        stats.ecran = {};
        stats.ecran.largeur = null;
        stats.ecran.hauteur = null;
        
        // clicks
        stats.clic = {};
        stats.clic.num = 0;   // variable pour le numéro du clic
        
        // Liste des éléments suivis pourles stats
        // pour faciliter le suivi des stats
        stats.listeElements = [];
        
    //===========================================================
    // initialisation des statistiques
    //===========================================================
    function initStats(nomPage, versionPage){
        /*---------------------------------------------------------------------------------
            function initStats()
            
            Cette fonction permet d'initialiser certaines variables nécessaires pour les
            statistiques.
            notamment l'id de la page, la taille de l'écran, ...
            
            Entrées : 
            ---------
                nomPage     : une chaîne de caractères avec le nom de la  
                              page dans laquelle l'utilisateur a cliqué.
                versionPage : la version de la page. utile notamment pour les tests A/B pour
                              améliorer les pages.
            Sorties :
            ---------
            
            Notes : 
            -------
        ---------------------------------------------------------------------------------*/
    
        // debug
        infoDebug("log", "initStats()");
    
        // mémorise l'id de la page
        stats.nomPage = nomPage;
        
        // mémorise la version de la page
        stats.versionPage = versionPage;
        
        // récupère les données sur la taille de l'écran
        stats.ecran.largeur  = $(window).width();
        stats.ecran.hauteur  = $(window).height();
        
        // récupère le referrer
        stats.referrer = document.referrer;
        
        // récupère le type de navigateur de l'utilisateur
        //================================================
        var ua = navigator.userAgent;
        if(ua.indexOf("Firefox")){
            stats.nav = "Firefox";
        } else if(ua.indexOf("Chrome")){
            stats.nav = "Chrome";
        } else if(ua.indexOf("Opera" )){
            stats.nav = "Opera";
        } else if(ua.indexOf("Safari")){
            stats.nav = "Safari";
        } else if(ua.indexOf("MSIE"  )){
            stats.nav = "IExplorer";
        }
        
        // Appzl à statUser, mais attend un peu pour être sûr d'avoir récupéré la taille écran
        setTimeout(statUser, 100);      
    } //function initStats()
    
    //===========================================================
    // fonction permettant d'ajouter un nouvel évènement statClic
    //===========================================================
    function addStatClicEvent(element, priorite, nom, param1, param2, lienSortie){
        /*-------------------------------------------------------------------------------------------------------
            function addStatClicEvent()
            
            Cette fonction permet de créer un event associé à un élément dont on veut enregistrer 
            le clic en base de donnée. Elle crée l'évènement ainsi que la callback associée.
        
            Entrées : 
            ---------
                priorite    : la priorité associée à l'évènement {1, 2, 3}
                              selon le mode choisi, seul les évènement de priorité inférieure
                              à une valeur données seront envoyées au serveur pour être enregistrées.
                nom         : le nom de l'élément pour lequel on veut enregistrer les clics.
                              NB: Plus exactement il s'agit de la chaîne de caractère définie dans l'attribut sc-nom
                                  de l'élément pour lequel on veut enregistrer les clics.
                param1      : le 1er paramètre
                param2      : le second paramètre
                lienSortie  : true si l'élément est un lien de sortie de la page, false sinon.
                
            Sorties : 
            ---------
                 pas de sorties
        ------------------------------------------------------------------------------------------------------------*/
    
        // debug
        infoDebug("log", "addStatClicEvent()" );

        // préparation de la callback associée à l'évènement.
        // NB: cette callback est un peu compliquée car elle doit pouvoir recevoir des paramètres.
        //     Et comme les callback n'en sont pas capables, il faut une closure pour contourner le problème.
        
        function statClicCreateCallback(priorite, nom, param1, param2, lienSortie) {
            // cette fonction renvoie un pointeur vers une callback qui appelle
            // la fonction statClicReveiver avec les bons paramètres.
            return function() {
                /*---------------------------------------------------
                    Cette fonction anonyme est une closure.
                    
                    C'est à dire qu'on la construit avec certains paramètres, 
                    Ca permet de fabriquer une callback avec des paramètres, 
                    ce qui n'est pas possible sinon
                    
                    Entrées : 
                    ---------
                        priorite    : la priorité associée à l'évènement {1, 2, 3}
                                      selon le mode choisi, seul les évènement de priorité inférieure
                                      à une valeur données seront envoyées au serveur pour être enregistrées.
                        nom         : Le nom de l'élément pour lequel on veut enregistrer les clics.
                                      NB: Plus exactement il s'agit de la chaîne de caractère définie dans l'attribut sc-nom
                                          de l'élément pour lequel on veut enregistrer les clics.
                        param1      : le 1er paramètre
                        param2      : le second paramètre
                        lienSortie  : true si l'élément est un lien de sortie de la page, false sinon.
                        
                    Sortie : 
                    --------
                        un pointeur vers la callback qui aura les bons paramètres
                ----------------------------------------------------*/

                // appel à la fonction statClicReceiver
                statClicReceiver(priorite, nom, param1, param2, lienSortie);
            } // fonction anonyme
        } // function statClicCreateCallback()
    
        // création de l'évènement
        $(element).click(statClicCreateCallback(priorite, nom, param1, param2, lienSortie));
    
        // debug
        infoDebug("stats", "addStatClicEvent() : nom = " +nom+"; param1  = " +param1+"; param2  = " +param2+"; lienSortie = "+lienSortie);
        if($(element).length == 0){
            errorDebug("addStatClicEvent() => l'élément n'existe pas !");
        } 
    } // function addStatClicEvent()
    
    //===========================================================
    // fonction appelée lors d'un clic sur un élément pour lequel on veut
    // enregistrer les clics dans la base de donnée.
    //===========================================================
    function statClicReceiver(priorite, nom, param1, param2, lienSortie){
        /*-------------------------------------------------------
            function statClicReceiver()
            
            Cette fonction permet d'envoyer au serveur des statistiques sur ce que les
            utilisateurs cliquent. Elle est appelée quand un élément dont on veut enregsitrer
            les clics en base de donnée, est cliqué.
            
            Entrées : 
            ---------
                priorite    : {1, 2, 3} la priorité de l'info.
                              1 : à enregistrer systématiquement
                              2 et 3 : à enregistrer ou pas selon le paramétrage général.
                nom         : le nom de l'élément pour lequel on veut enregistrer les clics.
                              NB: Plus exactement il s'agit de la chaîne de caractère définie dans l'attribut sc-nom
                                  de l'élément pour lequel on veut enregistrer les clics.
                param1      : Le paramètre de l'élément cliqué, s'il y en a besoin 
                              Par exemple, si l'élément cliqué est un lien pour acheter un livre, 
                              le paramètre serait l'id du livre. 
                              Ou si l'élément cliqué est un filtre, le paramètre serait le type de filtre. 
                              Ou si c'est un coach, le paramètre serait l'id du coach…
                param2      : Pour le cas où il y en aurait besoin
                lienSortie  : (booleen) indique si l'élément cliqué est un lien vers une autre page.
            
            Sorties :
            ---------
            
            Notes : 
            -------
        ---------------------------------------------------------*/
    
        // debug
        infoDebug("log", "statClicReceiver()");

        // si l'info n'est pas assez prioritaire pour être enregistrée
        if(priorite > stats.priorite){return;}
    
        // mise à jour du nombre de clics pour cette page
        stats.clic.num++;
        
        // conversion des paramètres en chaînes de caractères
        
        // preparation d'un objet à envoyer au serveur
        var objClic = {}
        objClic.sc_nomPage      = stats.nomPage;
        objClic.sc_versionPage  = stats.versionPage;
        objClic.sc_largeurEcran = stats.ecran.largeur;
        objClic.sc_hauteurEcran = stats.ecran.hauteur;
        objClic.sc_nom          = nom;
        objClic.sc_param1       = param1;
        objClic.sc_param2       = param2;
        objClic.sc_lienSortie   = lienSortie;
        objClic.sc_priorite     = priorite;
        objClic.sc_numClic      = stats.clic.num;
        
        // debug
        infoDebug("test", "éléments envoyés :");
        if((typeof(option) != "undefined") && option.debug && (option.debugCat["test"] == true)){
            console.log(objClic);
        }

        // envoi des infos au serveur
        if((typeof(option) != "undefined") && option.debug){
            // version qui affiche des infos de debug dans la console
            sendToServer(ajaxCall.root, ajaxCall.statClic, objClic, testDonneesRecues);

            //debug
            infoDebug("log", "statClicReceiver : version avec debug");
        }
        else{
            // version de production, qui n'envoie pas d'infos de debug
            sendToServer(ajaxCall.root, ajaxCall.statClic, objClic);
        }
    } // function statClicReceiver()

    //===========================================================
    // Mise à jour des évènements pour les stats de clics
    //===========================================================
    function majStatClic(){
        /*------------------------------------------------------------------------------------------------------------------------
            function majStatClic()
            
            Cette fonction repère, parmi les éléments de classe ".statClic", ceux auxquels on n'a pas encore
            associé d'évènement pour déclencher l'enregistrerment d'une statistique, puis leur associe l'évènement.
            
            Il est utile d'appeler cette fonction après le chargement de la page, mais également après chaque modification
            de la page. En effet, lors d'une modification de la page, un script pourrait avoir ajouté des élément que l'on veut
            prendre en compte dans les statistiques.
            
            Pas d'entrées, ni de sorties
        ------------------------------------------------------------------------------------------------------------------------*/
    
        // debug
        infoDebug("log", "majStatClic()");
        
        // si les stats ne doivent pas être renvoyées
        if(tlcParam.stats.statClic == OFF){return;} // on quitte la fonction

        // définition des variables
        var newEv, nom, param1, param2, lienSortie, priorite, typeParam1, typeParam2;
        
        // partie en jQuery, permet d'attendre que la page soit chargée avant d'exécuter le jQuery
        $(function(){
            // recherche les nouveau évènements à traquer dans les stats, c'est à dire ceux  qui sont à traquer dans 
            // les statistiques, mais pour lesquels on n'a pas encore ajouté l'évènement de stat.
            newEv = $(".statClic:not(.sc-ev-OK)"); 
            
            // debug
            infoDebug("test", "majStatClic() : "+newEv.length+" nouveaux éléments");            
            
            // boucle for sur l'ensemble des éléments de newEv
            newEv.each(function(){
                // récupération des attributs nécessaires pour les stats
                nom         = $(this).attr("sc-nom");
                param1      = $(this).attr("sc-param1");
                param2      = $(this).attr("sc-param2");
                lienSortie  = $(this).attr("sc-lienSortie");
                priorite    = $(this).attr("sc-priorite");
                typeParam1  = $(this).attr("sc-typeParam1");
                typeParam2  = $(this).attr("sc-typeParam2");
                
                // traitement des attributs extraits
                //==================================
                
                // nom
                if((typeof(nom) == "undefined") && (typeof(option) != "undefined") && option.debug){
                    errorDebug("majStatClic() : L'attribut 'nom' n'est pas défini => élément non pris en compte dans les stats");
                    return; // on quitte la boucle newEv.each(), pour cet élément de la boucle.
                } // if(typeof(nom...)
                
                // lien de sortie
                if(typeof(lienSortie) == "undefined"){
                    lienSortie = false;
                } else if(lienSortie == "true"){
                    lienSortie = true;
                } else if(lienSortie == "false"){
                    lienSortie = false;
                } else { // s'il est défini mais à une mauvaise valeur
                    // on considère que l'utilisateur voulait signifier qu'il s'agit bien d'un lien de sortie.
                    lienSortie = true;
                }// if lienSortie
                
                // param1
                if((typeof(typeParam1) != "undefined") && (typeof(param1) != "undefined")){
                    warnDebug("majStatClic() : param1 et typeParam1 sont tous les 2 définis => priorité à typeParam1");
                } // if((typeof(typeParam1)
                
                switch(typeParam1){
                    case "text":
                        param1 = $(this).text();
                        break;
                    case "val":
                        param1 = $(this).val();
                        break;
                } // switch
                
                if(typeof(param1) == "undefined"){
                    param1 = "."; // chaîne puor indiquer qu'il n'y a pa de param
                } // if(typeof(param1)


                // param2
                if((typeof(typeParam2) != "undefined") && (typeof(param2) != "undefined")){
                    warnDebug("majStatClic() : param2 et typeParam2 sont tous les 2 définis => priorité à typeParam2");
                } // if((typeof(typeParam1)
                
                switch(typeParam2){
                    case "text":
                        param2 = $(this).text();
                        break;
                    case "val":
                        param2 = $(this).val();
                        break;
                } // switch
                
                if(typeof(param2) == "undefined"){
                    param2 = "."; // chaîne puor indiquer qu'il n'y a pa de param
                } // if(typeof(param1)

                // priorite
                if(typeof(priorite) == "undefined"){
                    priorite = 5;
                } else {
                    priorite = parseInt(priorite);
                } // if(typeof(priorite)
                
                // ajout de l'évènement permettant d'enregistrer un clic sur cet élément
                addStatClicEvent(this, priorite, nom, param1, param2, lienSortie);
                
                // enregistrement de l'élément suivi dans une liste afin de pouvoir afficher facilement
                // la liste et faire un suivi plus facile des statistiques.
                if((typeof(option) != "undefined") && option.debug){
                    var element = {};
                    element.priorite    = priorite;
                    element.nom         = nom;
                    element.param1      = param1;
                    element.param2      = param2;
                    element.lienSortie  = lienSortie;
                    stats.listeElements.push(element);
                } // if((typeof(option)   enregistrement de l'élément dans une varaible globale
                
                // On ajoute la classe .sc-ev-OK, pour signifier que cet élément est maintenant pris en compte
                // et qu'il a son évènement associé pour enregistrer les clics.
                $(this).addClass("sc-ev-OK");
                
            }); // // boucle for sur l'ensemble des éléments de newEv
        }); // partie en jQuery
    } // function majStatClic()

    //===========================================================
    // stats sur les utilisateurs
    //===========================================================
    function statUser(){
        /*------------------------------------------------------------------------------------------------------------------------
            function statUser()
            
            Cette fonction enregistrer des statistiques sur les utilisateurs dans la base de données..
            NB: initStats(); doit avoir été lancé avant d'appeler statUser().
            
            Pas d'entrées, ni de sorties
        ------------------------------------------------------------------------------------------------------------------------*/
        
        // debug
        infoDebug("log", "statUser()");
        
        // vérifie s'il faut shunter cette fonction
        if(tlcParam.stats.statUser == OFF){return;}

        // preparation d'un objet à envoyer au serveur
        //============================================
        // 'su_largeurEcran', 'su_hauteurEcran', 'Su_referrer' , 'su_nav', 'su_nomPage'
        var objToSend = {}
        objToSend.su_nomPage        = stats.nomPage;
        objToSend.su_versionPage    = stats.versionPage;
        objToSend.su_referrer       = stats.referrer;
        objToSend.su_nav            = stats.nav;
        objToSend.su_largeurEcran   = stats.ecran.largeur;
        objToSend.su_hauteurEcran   = stats.ecran.hauteur;
        
        // debug
        infoDebug("test", "éléments envoyés :");
        if((typeof(option) != "undefined") && option.debug && (option.debugCat["test"] == true)){
            console.log(objToSend);
        }

        // envoi des infos au serveur
        if((typeof(option) != "undefined") && option.debug){
            // version qui affiche des infos de debug dans la console
            sendToServer(ajaxCall.root, ajaxCall.statUser, objToSend, testDonneesRecues);
        }
        else{
            // version de production, qui n'envoie pas d'infos de debug
            sendToServer(ajaxCall.root, ajaxCall.statUser, objToSend);
        }
    } // function statUser()
    
//###############################################################
// tri
//###############################################################

    //================================================================================================================
    // tri d'un tableau
    //================================================================================================================
    function sort(tableau){
        /*--------------------------------------------------------------------------------------------------------
            function sort(tableau)
            
            Cette fonction trie un tableau et fournit le nouvel ordre
            
            Entrées : 
            ---------
                tableau : le tableau à trier. Ce peut être des nombres ou des chaînes de caractères.
                
            Sorties : 
            ---------
                ordre : l'ordre des éléments du tableau pour que le tableau soit trié
                
            Notes : 
            -------
            Validé le 2019 08 02
        --------------------------------------------------------------------------------------------------------*/
    
        // debug
        infoDebug("log", "sort()");
    
        // initialisation du tableau de l'ordre
        //=====================================
        // à la fin, ordre contient [0, 1, 2, ..., tableau.length]
        var ordre = [];
        for(num = 0; num < tableau.length; num++){
            ordre.push(num);
        } // for(num...)
        
        
        // met en place le tri
        //=====================
        
        // variable pour les switchs
        var temp;
        
        // init
        shouldSwitch = false;       
        // fait en sorte que la boucle continue jusqu'à ce qu'il n'y ait plus de switch
        switching = true;
        while (switching) {
            // init
            switching = false;
            // boucle sur tous les éléments du tableau
            for (i = 0; i < (tableau.length - 1); i++) {
                // init
                shouldSwitch = false;
                // récupère les 2 éléments à comparer
                x = tableau[ordre[i]];
                y = tableau[ordre[i+1]]
                // Vérifie si les 2 éléments doivent être échangés
                if (x > y) {
                // si oui, on le note avec shouldSwitch et on arrête la boucle for
                // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
                } // if (x...)
            } // for (i...)
            if (shouldSwitch) {
                // Si on a noté qu'il faut switcher les 2 éléments, on fait l'échange et on indique 
                // qu'un switch a été fait, ce qui permet de continuer la boucle while
                temp = ordre[i];
                ordre[i] = ordre[i+1];
                ordre[i+1] = temp;
                switching = true;
            } // if (shouldSwitch)
        } // while (switching)  
        
        // renvoie du tableau avec le bon ordre
        return ordre;
    } //function sort()


