// dev-parametres.js
// ce fichier permet de définir des paramètres pour le fonctionnement des différents scripts Javascript
// il s'agit de la version de développement

//===============================================================
// Paramètres généraux, partagés avec d'autres scripts js
//===============================================================

// définition de constantes pour faciliter la lecture des options
const ON  = true;
const OFF = false;

// gestion des options et du debug
// NB: en mode production, la variable globale "option" n'existe pas !
var option = {
    debug       : true,         // pour afficher les infos de débug
    debugCat:  new Map([  // pour afficher les infos de débug catégorie par catégorie
        ["log"       , true  ],
        ["test"      , true  ],
        ["chaineJson", true ],
        ["temp"      , true  ],
        ["info"      , true  ],
        ["stats"     , true  ]
    ]), // debugCat
    serverOff   : false,        // pour fonctionner sans serveur, en simulant les résultats de requêtes avec un fichier json
    jQueryLocal : false,        // pour utiliser un jQuery local plutôt que hébergé sur un serveur donné.
                                // NB: à priori, ça fonctionne avec une version distribuée par un serveur, 
                                // même si le navigateur bloque le stockage local.
    test        : true          // pour afficher les résultats de quelques tests
}; // option

// paramètres de fonctionnement du site
var tlcParam = {
    stats: {
        priorite    : 5,        // priorité des infos  en deça de laquelle à envoyer à la base de données.
        statClic    : ON,       // envoi des stats lors des clics
        statPos     : ON,       // envoi des stats des positions
        statUser    : ON,       // envoi des stats sur les utilisateurs
    } // stats
}; //
