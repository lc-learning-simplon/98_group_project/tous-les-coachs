<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
$session = \Config\Services::session();

class Admin implements FilterInterface
{
    public function before(RequestInterface $request)
    {
        // Do something here        
       
        if (isset($_SESSION['role_id'])) {
            

            if ($_SESSION['role_id'] != 1) {

                return redirect()->back()->with('error','vous ne disposez pas des droits administrateurs');
            }
            return $request;

        }
     
        
        return redirect()->back()->with('error','vous devez vous connecter');
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response)
    {
        // Do something here
    }
}
