<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
$session = \Config\Services::session();

class Coach implements FilterInterface
{
    public function before(RequestInterface $request)
    {
        // Do something here        
       
        if (isset($_SESSION['role_id'])) {
            

            if ($_SESSION['role_id'] != 2) {

                return redirect()->back()->with('error','Vous n\'êtes pas un coach.');
            }
            return $request;

        }
     
        
        return redirect()->back()->with('error','Vous devez vous connecter.');
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response)
    {
        // Do something here
    }
}
