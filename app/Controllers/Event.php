<?php namespace App\Controllers;
$session = \Config\Services::session();
$session->start();


class Event extends BaseController
{
	public function index()
	{  
		echo view('templates/themeHeader');
        echo view('pages/event');
        echo view('templates/themeFooter');
	}

}
