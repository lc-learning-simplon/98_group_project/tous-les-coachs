<?php

namespace App\Controllers;

use App\Models\RoleModel;
use App\Models\UserModel;
use App\Models\CoachModel;
use App\Models\CategoryModel;
use App\Models\UserRoleModel;
use App\Models\CoachCategoryModel;

$session = \Config\Services::session();


class Profil extends BaseController
{

    /**
     * Méthode pour afficher le profil de l'utilisateur connecté.
     */

    public function index()
    {

        if (isset($_SESSION['user_id'])) {

            $userModel = new UserModel;
            $user = $userModel->find($_SESSION['user_id']);

            $userRoleModel = new UserRoleModel;
            $role_id = $userRoleModel->where('user_id', $_SESSION['user_id'])->findColumn('role_id');
            $RoleModel = new RoleModel;
            $role = $RoleModel->where('role_id', $role_id)->findColumn('slug');

            if ($role[0] == 'Coach') {
                $coachModel = new CoachModel;
                $coach = $coachModel->where('user_id', $_SESSION['user_id'])->first();

                foreach ($coach as $key => $value) {
                    $user[$key] = $value;
                }
            }

            $data = [
                'user'      =>  $user,
                'role'      =>  $role
            ];


            echo view('profil/profil', $data);
        } else {
            $session = session();
            $_SESSION['error'] = "Vous n'êtes pas connecté";
            $session->markAsFlashdata('error');
            return redirect()->to('/home')->with('error', 'Vous n\'êtes pas connecté.');
        }
    }


    /**
     * Méthode pour afficher le formulaire d'édition du profil de l'utilisateur connecté.
     */

    public function update()
    {

        $userModel = new UserModel;
        $user = $userModel->find($_SESSION['user_id']);

        if($_SESSION['role_id'] == 2) {
            $coachModel = new CoachModel;
            $coach = $coachModel->where('user_id', $_SESSION['user_id'])->first();
    
            foreach ($coach as $key => $value) {
                $user[$key] = $value;
            }
    
            $coachCategoryModel = new CoachCategoryModel;
            $categories = $coachCategoryModel->where('coach_id', $coach['coach_id'])->findColumn('category_id');
            $categories_id = [];
            if(isset($categories)) {
                foreach($categories as $category_id) {
                    $categories_id[$category_id] = $category_id;
                }
            }
    
            $categoryModel = new CategoryModel;
            $category = $categoryModel->findAll();
    
    
            $data = [
                'user'          => $user,
                'categories_id' => $categories_id,
                'categories'    => $category
            ];

        }

        else {
            $data = [
                'user'          => $user
            ];
        }


        echo view('profil/update', $data);
    }

    /**
     * Méthode pour sauvegarder le profil de l'utilisateur suite à la validation du formulaire.
     */

    public function save()
    {

        $userModel = new UserModel();
        $data = [
            'first_name'    => $_POST['first_name'],
            'last_name'    => $_POST['last_name'],
            'email'    => $_POST['email']
        ];
        $userModel->update($_SESSION['user_id'], $data);

        $_SESSION['first_name'] = $_POST['first_name'];
        $_SESSION['last_name'] = $_POST['last_name'];

        if($_SESSION['role_id'] == 2) {
            // Appel API pour lng et lat [developer.mapquest.com]
    
            $vowels = array("é", "è", "ê", "ë");
            $address = str_replace($vowels, "e", str_replace(" ", "+", $_POST['address'] . " " . $_POST['city']));
    
            $json = file_get_contents("https://www.mapquestapi.com/geocoding/v1/address?key=" . getenv('API_KEY_MAPQUEST') . "&inFormat=kvp&outFormat=json&location=$address%2CFR&thumbMaps=false");
            $json = json_decode($json);
    
            $lat = $json->{'results'}[0]->{'locations'}[0]->{'latLng'}->{'lat'};
            $lng = $json->{'results'}[0]->{'locations'}[0]->{'latLng'}->{'lng'};
    
            $coachModel = new CoachModel();
            $data = [
                'address'    => $_POST['address'],
                'geocod_lat' => $lat,
                'geocod_lng' => $lng,
                'city'       => $_POST['city'],
                'postcode'   => $_POST['postcode'],
                'country'    => $_POST['country'],
                'phone'      => $_POST['phone'],
                'facebook'   => $_POST['facebook'],
                'twitter'    => $_POST['twitter'],
                'linkedin'   => $_POST['linkedin'],
                'website'    => $_POST['website'],
                'title'      => $_POST['title'],
                'description' => $_POST['description']
            ];
            $coach_id = $coachModel->where('user_id', $_SESSION['user_id'])->findColumn('coach_id');
            $coachModel->update($coach_id[0], $data);
    
            // Catégories
            $coachCategoryModel = new CoachCategoryModel;
            $coachCategories = $coachCategoryModel->where('coach_id', $coach_id)->findAll();
    
            $categories = [];
    
            foreach($coachCategories as $coachCategory) {
                $categories[] = $coachCategory['category_id'];
            }
    
    
            foreach($_POST['categories'] as $category_id) {
                if(in_array($category_id, $categories)) {
                    // Nothing
                }
                elseif($category_id == 0) {
                    // Nothing
                }
                else {
                    // Insert
                    $data = [
                        'coach_id'      =>  $coach_id,
                        'category_id'   =>  $category_id
                    ];
                    $coachCategoryModel->insert($data);
                }
            }
    
            foreach($categories as $category) {
                if(!in_array($category, $_POST['categories'])) {
                    $coachCategoryModel->where('coach_id', $coach_id)->where('category_id', $category)->delete();
                }
            }
        }

        
        return redirect()->to('/profil')->with('success', 'Vos modifications ont bien été prise en compte.');
    }

    /**
     * Méthode pour changer le mot de passe.
     */

    public function savepwd()
    {
        $session = session();
        $userModel = new UserModel;

        $user_pwd = $userModel->where('user_id', $_SESSION['user_id'])->findColumn('password');

        $post_pwd = hash('sha384', "$_POST[password]");

        if ($user_pwd[0] != $post_pwd) {

            return redirect()->to('/profil/update')->with('error', 'Votre mot de passe est incorrecte.');
        } else {

            $data = [
                'password' => hash(getenv('HASH'), "$_POST[newpassword1]")
            ];
            $userModel->update($_SESSION['user_id'], $data);

            return redirect()->to('/profil')->with('success', 'Votre mot de passe à bien été modifié.');
        }
    }

    public function upload()
    {
        $dossier = 'images/avatars/';
        $fichier = basename('avatar-user-' . $_SESSION['user_id']);
        $taille_maxi = 2000000;
        $taille = filesize($_FILES['avatar']['tmp_name']);
        $extensions = array('.png', '.gif', '.jpg', '.jpeg');
        $extension = strrchr($_FILES['avatar']['name'], '.');

        //Début des vérifications de sécurité...
        if (!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
        {
            return redirect()->to('/profil/update')->with('error', 'Vous devez uploader un fichier de type png, gif, jpg ou jpeg.');
        } else {
            if ($taille > $taille_maxi || $taille == false) {
                return redirect()->to('/profil/update')->with('error', 'Le fichier est trop volumineux.');
            } else //S'il n'y a pas d'erreur, on upload
            {
                //On formate le nom du fichier ici...
                $fichier = strtr(
                    $fichier,
                    'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
                    'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy'
                );
                $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);
                if (move_uploaded_file($_FILES['avatar']['tmp_name'], $dossier . $fichier . $extension)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
                {

                    $userModel = new UserModel;
                    $data = [
                        'picture' => '/images/avatars/avatar-user-' . $_SESSION['user_id'] . $extension
                    ];
                    $userModel->update($_SESSION['user_id'], $data);

                    $_SESSION['picture'] = $data['picture'];

                    return redirect()->to('/profil/update')->with('success', 'Photo uploadé.');
                } else //Sinon (la fonction renvoie FALSE).
                {
                    return redirect()->to('/profil/update')->with('error', 'Echec de l\'upload.');
                }
            }
        }
    }

    public function vitrine()
    {
        $userModel = new UserModel;
        $user = $userModel->find($_SESSION['user_id']);

        $userRoleModel = new UserRoleModel;
        $role_id = $userRoleModel->where('user_id', $_SESSION['user_id'])->findColumn('role_id');
        $RoleModel = new RoleModel;
        $role = $RoleModel->where('role_id', $role_id)->findColumn('slug');

        if ($role[0] == 'Coach') {
            $coachModel = new CoachModel;
            $coach = $coachModel->where('user_id', $_SESSION['user_id'])->first();
            
            // Categories
            $coachCategoryModel = new CoachCategoryModel;
            $CategoryModel = new CategoryModel;
    
            $categories_id = $coachCategoryModel->where('coach_id', $coach['coach_id'])->findColumn('category_id');
    
            if (is_array($categories_id)) {
                $categories = [];
                foreach ($categories_id as $category_id) {
                    $category_id = $CategoryModel->where('category_id', $category_id)->findColumn('name');
                    $categories[] = $category_id[0];
                }
                $coach['categories'] = $categories;
            } else {
                $coach['categories'][] = "Non précisé";
            }

            foreach ($coach as $key => $value) {
                $user[$key] = $value;
            }
        }


        $data = [
            'coach'      =>  $user,
            'role'      =>  $role
        ];


        echo view('profil/vitrine', $data);
    }
}
