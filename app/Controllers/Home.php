<?php namespace App\Controllers;
$session = \Config\Services::session();
$session->start();


class Home extends BaseController
{
	public function index()
	{  
		echo view('templates/themeHeader');
        echo view('pages/home');
        echo view('templates/themeFooter');
	}

	public function page404()
	{  
		echo view('templates/themeHeader');
        echo view('pages/404');
        echo view('templates/themeFooter');
	}

}
