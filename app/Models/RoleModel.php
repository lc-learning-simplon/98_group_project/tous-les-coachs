<?php namespace App\Models;

use CodeIgniter\Model;

class RoleModel extends Model
{
        protected $table      = 'roles';
        protected $primaryKey = 'role_id';

        protected $returnType = 'array';
        protected $useSoftDeletes = false;

        protected $allowedFields = ['name', 'slug'];

        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        protected $validationRules    = [];
        protected $validationMessages = [];
        protected $skipValidation     = false;
}