<!DOCTYPE html>

<head>

    <!-- Basic Page Needs
================================================== -->
    <title>TOUSLESCOACHS.COM</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
================================================== -->
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/main-color.css" id="colors">

</head>

<body>

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header Container
================================================== -->
        <header id="header-container" class="fixed fullwidth dashboard">

            <!-- Header -->
            <div id="header" class="not-sticky">
                <div class="container">

                    <!-- Left Side Content -->
                    <div class="left-side">

                        <!-- Logo -->
                        <div id="logo">
                            <a href="/"><img src="/images/logo.png" alt=""></a>
                            <a href="/" class="dashboard-logo"><img src="/images/logo2.png" alt=""></a>
                        </div>

                        <!-- Mobile Navigation -->
                        <div class="mmenu-trigger">
                            <button class="hamburger hamburger--collapse" type="button">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>

                        <!-- Main Navigation -->
                        <nav id="navigation" class="style-1">
                            <ul id="responsive">


                                <li><a href="/">Accueil</a>
                                </li>

                                <li><a href="/coach">Coachs</a>
                                </li>

                                <li><a href="/livre">Livres</a>
                                </li>

                                <li><a href="/evenement">Evènements</a>
                                </li>

                                <li><a href="/formation">Formations de coaching</a>
                                </li>

                            </ul>
                        </nav>
                        <div class="clearfix"></div>
                        <!-- Main Navigation / End -->

                    </div>
                    <!-- Left Side Content / End -->

                    <!-- Right Side Content / End -->
                    <div class="right-side">
                        <div class="header-widget">

                            <?php if (isset($_SESSION['first_name'])) { ?>
                                <div class="user-menu">
                                    <div class="user-name"><span><img src="<?= $_SESSION['picture'] ?>" alt=""></span><?= "$_SESSION[first_name] $_SESSION[last_name]" ?></div>
                                    <ul>
                                        <li><a href="/profil"><i class="sl sl-icon-settings"></i>Mon compte</a></li>
                                        <?php if($_SESSION['role_id'] == 1){
											echo "<li><a href='/crud'><i class=' im im-icon-Administrator'></i> Espace admin</a></li>";
										};
										?>
                                        <li><a href="/logout"><i class="sl sl-icon-power"></i> Déconnexion</a></li>
                                    </ul>
                                </div>
                            <?php
                            } else {
                                ?>
                                <a href="#sign-in-dialog" class="sign-in popup-with-zoom-anim"><i class="sl sl-icon-login"></i> Se connecter</a>
                            <?php
                            }
                            ?>

                        </div>
                    </div>
                    <!-- Right Side Content / End -->

                </div>
            </div>
            <!-- Header / End -->

        </header>
        <div class="clearfix"></div>
        <!-- Header Container / End -->

        <!----------------------- COACH start ----------------------->
        <?php if ($_SESSION['role_id'] == 2) { ?>

            <!-- Dashboard -->
            <div id="dashboard">

                <!-- Navigation
	================================================== -->

                <!-- Responsive Navigation Trigger -->
                <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

                <div class="dashboard-nav">
                    <div class="dashboard-nav-inner">

                        <ul data-submenu-title="Account">
                            <li><a href="/profil"><i class="sl sl-icon-user"></i> Mon profile</a></li>
                            <li><a href="/profil/vitrine"><i class="sl sl-icon-people "></i> Vitrine</a></li>
                            <li><a href="/logout"><i class="sl sl-icon-power"></i> Déconnexion</a></li>
                        </ul>

                    </div>
                </div>
                <!-- Navigation / End -->


                <!-- Content
	================================================== -->
                <div class="dashboard-content">

                    <!-- Start Flash message success & error -->
                    <?php if (isset($_SESSION['error'])) { ?>
                        <div class="popup error" style="margin-bottom: 2rem;">
                            <p><?= $_SESSION['error'] ?></p>
                            <button class=" btn-popup" alt="close popup"></button>
                        </div>
                    <?php };
                        if (isset($_SESSION['success'])) { ?>
                        <div class="popup success" style="margin-bottom: 2rem;">
                            <p><?= $_SESSION['success'] ?></p>
                            <button class=" btn-popup" alt="close popup"></button>
                        </div>
                    <?php } ?>
                    <!-- End Flash message success & error -->

                    <div id="titlebar" class="gradient">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="user-profile-titlebar">
                                        <div class="user-profile-avatar"><img src="<?= $user['picture'] ?>" alt=""></div>
                                        <div class="user-profile-name">
                                            <h2><?= $user['first_name'] ?> <?= $user['last_name'] ?></h2>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="container">

                        <div style="display: flex; justify-content: flex-end;">
                            <a href="/profil/update" class="button" title="edit profil"><i class="fa fa-edit "></i>Editer</a>
                        </div>
                        <div class="row sticky-wrapper">


                            <div class="col-lg-4 col-md-4 margin-top-0">

                                <!-- Mes infos -->
                                <div class="boxed-widget margin-top-30 margin-bottom-50">
                                    <h3>Mes infos</h3>
                                    <ul class="listing-details-sidebar">
                                        <li><i class="sl sl-icon-phone"></i><?= $user['phone'] ?></li>
                                        <li><i class="fa fa-envelope-o"></i> <a href="#"><?= $user['email'] ?></a></li>
                                        <li><i class="sl sl-icon-home "></i><?= $user['address'] ?>, <?= $user['city'] ?>, <?= $user['country'] ?></li>
                                    </ul>

                                    <ul class="listing-details-sidebar social-profiles">
                                        <li><a href="<?= $user['facebook'] ?>" class="facebook-profile"><i class="fa fa-facebook-square"></i> Facebook</a></li>
                                        <li><a href="<?= $user['twitter'] ?>" class="twitter-profile"><i class="fa fa-twitter"></i> Twitter</a></li>
                                        <li><a href="<?= $user['linkedin'] ?>"><i class="fa fa-linkedin"></i> Linkedin</a></li>
                                        <li><a href="<?= $user['website'] ?>"><i class="fa fa-at"></i> Site Web</a></li>
                                    </ul>

                                </div>
                            </div>

                            <div class="col-lg-8 col-md-4 margin-top-0">

                                <!-- Description -->
                                <div class="boxed-widget margin-top-30 margin-bottom-50">
                                    <h3>Présentation</h3>
                                    <p><strong><?= $user['title'] ?></strong></p>
                                    <p><?= $user['description'] ?></p>
                                </div>
                            </div>
                        </div>

                        <!-- Copyrights -->
                        <div class="col-md-12">
                            <div class="copyrights">© 2019 Listeo. All Rights Reserved.</div>
                        </div>

                    </div>
                    <!-- Content / End -->


                </div>
                <!-- Dashboard / End -->

            <?php }

            // USER & ADMIN start

            else { ?>

                <!-- Dashboard -->
                <div id="dashboard">

                    <!-- Navigation
================================================== -->

                    <!-- Responsive Navigation Trigger -->
                    <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

                    <div class="dashboard-nav">
                        <div class="dashboard-nav-inner">

                            <ul data-submenu-title="Account">
                                <li><a href="/profil"><i class="sl sl-icon-user"></i> Mon profile</a></li>
                                <?php if($_SESSION['role_id'] == 1) { ?>
                                <li><a href="/crud"><i class="im im-icon-Administrator"></i> Admin</a></li>
                                <?php } ?>
                                <li><a href="/logout"><i class="sl sl-icon-power"></i> Déconnexion</a></li>
                            </ul>

                        </div>
                    </div>
                    <!-- Navigation / End -->


                    <!-- Content
================================================== -->
                    <div class="dashboard-content">

                        <!-- Start Flash message success & error -->
                        <?php if (isset($_SESSION['error'])) { ?>
                            <div class="popup error" style="margin-bottom: 2rem;">
                                <p><?= $_SESSION['error'] ?></p>
                                <button class=" btn-popup" alt="close popup"></button>
                            </div>
                        <?php };
                            if (isset($_SESSION['success'])) { ?>
                            <div class="popup success" style="margin-bottom: 2rem;">
                                <p><?= $_SESSION['success'] ?></p>
                                <button class=" btn-popup" alt="close popup"></button>
                            </div>
                        <?php } ?>
                        <!-- End Flash message success & error -->

                        <div id="titlebar" class="gradient">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="user-profile-titlebar">
                                            <div class="user-profile-avatar"><img src="<?= $user['picture'] ?>" alt=""></div>
                                            <div class="user-profile-name">
                                                <h2><?= $user['first_name'] ?> <?= $user['last_name'] ?></h2>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="container">

                            <div style="display: flex; justify-content: flex-end;">
                                <a href="/profil/update" class="button" title="edit profil"><i class="fa fa-edit "></i>Editer</a>
                            </div>
                            <div class="row sticky-wrapper">


                                <div class="col-lg-4 col-md-4 margin-top-0">

                                    <!-- Mes infos -->
                                    <div class="boxed-widget margin-top-30 margin-bottom-50">
                                        <h3>Mes infos</h3>
                                        <ul class="listing-details-sidebar">
                                            <li><i class="fa fa-envelope-o"></i> <a href="#"><?= $user['email'] ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <!-- Copyrights -->
                            <div class="col-md-12">
                                <div class="copyrights">© 2019 Listeo. All Rights Reserved.</div>
                            </div>

                        </div>
                        <!-- Content / End -->


                    </div>
                    <!-- Dashboard / End -->

                <?php } ?>

                </div>
                <!-- Wrapper / End -->

                <!-- Scripts
================================================== -->
                <script type="text/javascript" src="/scripts/jquery-3.4.1.min.js"></script>
                <script type="text/javascript" src="/scripts/jquery-migrate-3.1.0.min.js"></script>
                <script type="text/javascript" src="/scripts/mmenu.min.js"></script>
                <script type="text/javascript" src="/scripts/chosen.min.js"></script>
                <script type="text/javascript" src="/scripts/slick.min.js"></script>
                <script type="text/javascript" src="/scripts/rangeslider.min.js"></script>
                <script type="text/javascript" src="/scripts/magnific-popup.min.js"></script>
                <script type="text/javascript" src="/scripts/waypoints.min.js"></script>
                <script type="text/javascript" src="/scripts/counterup.min.js"></script>
                <script type="text/javascript" src="/scripts/jquery-ui.min.js"></script>
                <script type="text/javascript" src="/scripts/tooltips.min.js"></script>
                <script type="text/javascript" src="/scripts/custom.js"></script>
                <script type="text/javascript" src="/scripts/popup-redirect.js"></script>


                <!-- Google Autocomplete -->
                <script>
                    function initAutocomplete() {
                        var input = document.getElementById('autocomplete-input');
                        var autocomplete = new google.maps.places.Autocomplete(input);

                        autocomplete.addListener('place_changed', function() {
                            var place = autocomplete.getPlace();
                            if (!place.geometry) {
                                return;
                            }
                        });

                        if ($('.main-search-input-item')[0]) {
                            setTimeout(function() {
                                $(".pac-container").prependTo("#autocomplete-container");
                            }, 300);
                        }
                    }
                </script>
                <script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initAutocomplete"></script>


                <!-- Typed Script -->
                <script type="text/javascript" src="/scripts/typed.js"></script>
                <script>
                    var typed = new Typed('.typed-words', {
                        strings: ["Attractions", " Restaurants", " Hotels"],
                        typeSpeed: 80,
                        backSpeed: 80,
                        backDelay: 4000,
                        startDelay: 1000,
                        loop: true,
                        showCursor: true
                    });
                </script>


                <!-- Date Range Picker - docs: http://www.daterangepicker.com/ -->
                <script src="/scripts/moment.min.js"></script>
                <script src="/scripts/daterangepicker.js"></script>

                <script>
                    $(function() {

                        var start = moment().subtract(0, 'days');
                        var end = moment().add(2, 'days');

                        function cb(start, end) {
                            $('#booking-date-search').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                        }
                        cb(start, end);
                        $('#booking-date-search').daterangepicker({
                            "opens": "right",
                            "autoUpdateInput": true,
                            "alwaysShowCalendars": true,
                            startDate: start,
                            endDate: end
                        }, cb);

                        cb(start, end);

                    });

                    // Calendar animation and visual settings
                    $('#booking-date-search').on('show.daterangepicker', function(ev, picker) {
                        $('.daterangepicker').addClass('calendar-visible calendar-animated bordered-style');
                        $('.daterangepicker').removeClass('calendar-hidden');
                    });
                    $('#booking-date-search').on('hide.daterangepicker', function(ev, picker) {
                        $('.daterangepicker').removeClass('calendar-visible');
                        $('.daterangepicker').addClass('calendar-hidden');
                    });

                    $(window).on('load', function() {
                        $('#booking-date-search').val('');
                    });
                </script>



</body>

</html>