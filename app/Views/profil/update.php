<!DOCTYPE html>

<head>

    <!-- Basic Page Needs
================================================== -->
    <title>TOUSLESCOACHS.COM</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
================================================== -->
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/main-color.css" id="colors">

</head>

<body>

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header Container
================================================== -->
        <header id="header-container" class="fixed fullwidth dashboard">

            <!-- Header -->
            <div id="header" class="not-sticky">
                <div class="container">

                    <!-- Left Side Content -->
                    <div class="left-side">

                        <!-- Logo -->
                        <div id="logo">
                            <a href="/"><img src="images/logo.png" alt=""></a>
                            <a href="/" class="dashboard-logo"><img src="/images/logo2.png" alt=""></a>
                        </div>

                        <!-- Mobile Navigation -->
                        <div class="mmenu-trigger">
                            <button class="hamburger hamburger--collapse" type="button">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>

                        <!-- Main Navigation -->
                        <nav id="navigation" class="style-1">
                            <ul id="responsive">


                                <li><a href="/">Accueil</a>
                                </li>

                                <li><a href="/coach">Coachs</a>
                                </li>

                                <li><a href="/livre">Livres</a>
                                </li>

                                <li><a href="/evenement">Evènements</a>
                                </li>

                                <li><a href="/formation">Formations de coaching</a>
                                </li>

                            </ul>
                        </nav>
                        <div class="clearfix"></div>
                        <!-- Main Navigation / End -->

                    </div>
                    <!-- Left Side Content / End -->

                    <!-- Right Side Content / End -->
                    <div class="right-side">
                        <div class="header-widget">

                            <?php if (isset($_SESSION['first_name'])) { ?>
                                <div class="user-menu">
                                    <div class="user-name"><span><img src="<?= $_SESSION['picture'] ?>" alt=""></span><?= "$_SESSION[first_name] $_SESSION[last_name]" ?></div>
                                    <ul>
                                        <li><a href="/profil"><i class="sl sl-icon-settings"></i>Mon compte</a></li>
                                        <?php if($_SESSION['role_id'] == 1){
											echo "<li><a href='/crud'><i class=' im im-icon-Administrator'></i> Espace admin</a></li>";
										};
										?>
                                        <li><a href="/logout"><i class="sl sl-icon-power"></i> Déconnexion</a></li>
                                    </ul>
                                </div>
                            <?php
                            } else {
                                ?>
                                <a href="#sign-in-dialog" class="sign-in popup-with-zoom-anim"><i class="sl sl-icon-login"></i> Se connecter</a>
                            <?php
                            }
                            ?>

                        </div>
                    </div>
                    <!-- Right Side Content / End -->

                </div>
            </div>
            <!-- Header / End -->

        </header>
        <div class="clearfix"></div>
        <!-- Header Container / End -->

        <!----------------------- COACH start ----------------------->
        <?php if ($_SESSION['role_id'] == 2) { ?>

            <!-- Dashboard -->
            <div id="dashboard">

                <!-- Navigation
	================================================== -->

                <!-- Responsive Navigation Trigger -->
                <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

                <div class="dashboard-nav">
                    <div class="dashboard-nav-inner">

                        <ul data-submenu-title="Account">
                            <li><a href="/profil"><i class="sl sl-icon-user"></i> Mon profile</a></li>
                            <li><a href="/profil/vitrine"><i class="sl sl-icon-people "></i> Vitrine</a></li>
                            <li><a href="/logout"><i class="sl sl-icon-power"></i> Déconnexion</a></li>
                        </ul>

                    </div>
                </div>
                <!-- Navigation / End -->


                <!-- Content
	================================================== -->
                <div class="dashboard-content">

                    <!-- Start Flash message success & error -->
                    <?php if (isset($_SESSION['error'])) { ?>
                        <div class="popup error" style="margin-bottom: 2rem;">
                            <p><?= $_SESSION['error'] ?></p>
                            <button class=" btn-popup" alt="close popup"></button>
                        </div>
                    <?php };
                        if (isset($_SESSION['success'])) { ?>
                        <div class="popup success" style="margin-bottom: 2rem;">
                            <p><?= $_SESSION['success'] ?></p>
                            <button class=" btn-popup" alt="close popup"></button>
                        </div>
                    <?php } ?>
                    <!-- End Flash message success & error -->

                    <div class="row">

                        <!-- Profile -->
                        <div class="col-lg-6 col-md-12">
                            <div class="dashboard-list-box margin-top-0">
                                <h4 class="gray">Détails du profil</h4>
                                <div class="dashboard-list-box-static">

                                    <!-- Avatar -->
                                    <div class="edit-profile-photo">
                                        <img src="<?= $user['picture'] ?>" alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <form id="upload_form" method="post" enctype="multipart/form-data" action="/profil/upload">
                                                    <?= csrf_field() ?>
                                                    <span><i class="fa fa-upload"></i> Uploader Photo</span>
                                                    <input type="file" id="upload" class="upload" name="avatar" accept="image/png, image/jpeg" />
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        document.getElementById('upload').addEventListener('change', function() {
                                            document.getElementById('upload_form').submit();
                                        });
                                    </script>


                                    <!-- Details -->
                                    <div class="my-profile">

                                        <form action="/profil/save" method="POST">

                                            <?= csrf_field() ?>

                                            <label for="first_name">Prénom</label>
                                            <input value="<?= $user['first_name'] ?>" type="text" id="first_name" name="first_name">

                                            <label for="last_name">Nom</label>
                                            <input value="<?= $user['last_name'] ?>" type="text" id="last_name" name="last_name">

                                            <label for="phone">Télephone</label>
                                            <input value="<?= $user['phone'] ?>" type="text" id="phone" name="phone">

                                            <label for="email">E-mail</label>
                                            <input value="<?= $user['email'] ?>" type="email" id="email" name="email">


                                            <label>Catégories :</label>

                                            <select data-placeholder="Select Multiple Items" class="chosen-select" name="categories[]" multiple>
                                                <option value="0" selected></option>
                                                <?php foreach ($categories as $category) { ?>
                                                    <option value="<?= $category['category_id'] ?>" <?php if (isset($categories_id[$category['category_id']])) {
                                                                                                                echo "selected";
                                                                                                            } ?>><?= $category['name'] ?></option>
                                                <?php } ?>
                                            </select>

                                            <label for="address">Adresse</label>
                                            <input value="<?= $user['address'] ?>" type="text" id="address" name="address">

                                            <label for="city">Ville</label>
                                            <input value="<?= $user['city'] ?>" type="text" id="city" name="city">

                                            <label for="postcode">Code postal</label>
                                            <input value="<?= $user['postcode'] ?>" type="text" id="postcode" name="postcode">

                                            <label for="country">Pays</label>
                                            <input value="<?= $user['country'] ?>" type="text" id="country" name="country">

                                            <label for="title">Accroche</label>
                                            <input value="<?= $user['title'] ?>" type="text" id="title" name="title">

                                            <label for="description">Description</label>
                                            <textarea name="description" id="description" cols="30" rows="10"><?= $user['description'] ?></textarea>

                                            <label for="twitter"><i class="fa fa-twitter"></i> Twitter</label>
                                            <input placeholder="<?= $user['twitter'] ?>" type="text" id="twitter" name="twitter">

                                            <label for="facebook"><i class="fa fa-facebook-square"></i> Facebook</label>
                                            <input placeholder="<?= $user['facebook'] ?>" type="text" id="facebook" name="facebook">

                                            <label for="linkedin"><i class="fa fa-linkedin-square"></i> Linkedin</label>
                                            <input placeholder="<?= $user['linkedin'] ?>" type="text" id="linkedin" name="linkedin">

                                            <label for="website"><i class="fa fa-at"></i> Site Web</label>
                                            <input placeholder="<?= $user['website'] ?>" type="text" id="website" name="website">
                                    </div>

                                    <button class="button margin-top-15" title="save" type="submit">Sauvegarder</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- Change Password -->
                        <div class="col-lg-6 col-md-12">
                            <div class="dashboard-list-box margin-top-0">
                                <h4 class="gray">Changer de mot de passe</h4>
                                <div class="dashboard-list-box-static">

                                    <!-- Change Password -->
                                    <div class="my-profile">
                                        <form action="/profil/savepwd" method="POST" oninput='newpassword1.setCustomValidity(newpassword2.value != newpassword1.value ? "Les mots de passe ne correspondent pas." : "")'>

                                            <?= csrf_field() ?>

                                            <label class="margin-top-0" for="password">Mot de passe actuel</label>
                                            <input type="password" id="password" name="password">

                                            <label for="newpassword1">Nouveau mot de passe</label>
                                            <input type="password" id="newpassword1" name="newpassword1">

                                            <label for="newpassword2">Confirmer mot de passe</label>
                                            <input type="password" id="newpassword2" name="newpassword2">

                                            <button class="button margin-top-15" type="submit" title="change password">Changer de mot de passe</button>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Copyrights -->
                    <div class="col-md-12">
                        <div class="copyrights">© 2019 Listeo. All Rights Reserved.</div>
                    </div>

                </div>
                <!-- Content / End -->


            </div>
            <!-- Dashboard / End -->

        <?php }

        // USER & ADMIN start

        else { ?>

            <!-- Dashboard -->
            <div id="dashboard">

                <!-- Navigation
================================================== -->

                <!-- Responsive Navigation Trigger -->
                <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

                <div class="dashboard-nav">
                    <div class="dashboard-nav-inner">

                        <ul data-submenu-title="Account">
                            <li><a href="/profil"><i class="sl sl-icon-user"></i> Mon profile</a></li>
                            <?php if ($_SESSION['role_id'] == 1) { ?>
                                <li><a href="/crud"><i class="im im-icon-Administrator"></i> Admin</a></li>
                            <?php } ?>
                            <li><a href="/logout"><i class="sl sl-icon-power"></i> Déconnexion</a></li>
                        </ul>

                    </div>
                </div>
                <!-- Navigation / End -->


                <!-- Content
================================================== -->
                <div class="dashboard-content">

                    <!-- Start Flash message success & error -->
                    <?php if (isset($_SESSION['error'])) { ?>
                        <div class="popup error" style="margin-bottom: 2rem;">
                            <p><?= $_SESSION['error'] ?></p>
                            <button class=" btn-popup" alt="close popup"></button>
                        </div>
                    <?php };
                        if (isset($_SESSION['success'])) { ?>
                        <div class="popup success" style="margin-bottom: 2rem;">
                            <p><?= $_SESSION['success'] ?></p>
                            <button class=" btn-popup" alt="close popup"></button>
                        </div>
                    <?php } ?>
                    <!-- End Flash message success & error -->

                    <div class="row">

                        <!-- Profile -->
                        <div class="col-lg-6 col-md-12">
                            <div class="dashboard-list-box margin-top-0">
                                <h4 class="gray">Détails du profil</h4>
                                <div class="dashboard-list-box-static">

                                    <!-- Avatar -->
                                    <div class="edit-profile-photo">
                                        <img src="<?= $user['picture'] ?>" alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <form id="upload_form" method="post" enctype="multipart/form-data" action="/profil/upload">
                                                    <?= csrf_field() ?>
                                                    <span><i class="fa fa-upload"></i> Uploader Photo</span>
                                                    <input type="file" id="upload" class="upload" name="avatar" accept="image/png, image/jpeg" />
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        document.getElementById('upload').addEventListener('change', function() {
                                            document.getElementById('upload_form').submit();
                                        });
                                    </script>


                                    <!-- Details -->
                                    <div class="my-profile">

                                        <form action="/profil/save" method="POST">

                                            <?= csrf_field() ?>

                                            <label for="first_name">Prénom</label>
                                            <input value="<?= $user['first_name'] ?>" type="text" id="first_name" name="first_name">

                                            <label for="last_name">Nom</label>
                                            <input value="<?= $user['last_name'] ?>" type="text" id="last_name" name="last_name">

                                            <label for="email">E-mail</label>
                                            <input value="<?= $user['email'] ?>" type="email" id="email" name="email">

                                    </div>

                                    <button class="button margin-top-15" title="save" type="submit">Sauvegarder</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- Change Password -->
                        <div class="col-lg-6 col-md-12">
                            <div class="dashboard-list-box margin-top-0">
                                <h4 class="gray">Changer de mot de passe</h4>
                                <div class="dashboard-list-box-static">

                                    <!-- Change Password -->
                                    <div class="my-profile">
                                        <form action="/profil/savepwd" method="POST" oninput='newpassword1.setCustomValidity(newpassword2.value != newpassword1.value ? "Les mots de passe ne correspondent pas." : "")'>

                                            <?= csrf_field() ?>

                                            <label class="margin-top-0" for="password">Mot de passe actuel</label>
                                            <input type="password" id="password" name="password">

                                            <label for="newpassword1">Nouveau mot de passe</label>
                                            <input type="password" id="newpassword1" name="newpassword1">

                                            <label for="newpassword2">Confirmer mot de passe</label>
                                            <input type="password" id="newpassword2" name="newpassword2">

                                            <button class="button margin-top-15" type="submit" title="change password">Changer de mot de passe</button>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Copyrights -->
                    <div class="col-md-12">
                        <div class="copyrights">© 2019 Listeo. All Rights Reserved.</div>
                    </div>

                </div>
                <!-- Content / End -->


            </div>
            <!-- Dashboard / End -->

        <?php } ?>

    </div>
    <!-- Wrapper / End -->

    <!-- Scripts
================================================== -->
    <script type="text/javascript" src="/scripts/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="/scripts/jquery-migrate-3.1.0.min.js"></script>
    <script type="text/javascript" src="/scripts/mmenu.min.js"></script>
    <script type="text/javascript" src="/scripts/chosen.min.js"></script>
    <script type="text/javascript" src="/scripts/slick.min.js"></script>
    <script type="text/javascript" src="/scripts/rangeslider.min.js"></script>
    <script type="text/javascript" src="/scripts/magnific-popup.min.js"></script>
    <script type="text/javascript" src="/scripts/waypoints.min.js"></script>
    <script type="text/javascript" src="/scripts/counterup.min.js"></script>
    <script type="text/javascript" src="/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/scripts/tooltips.min.js"></script>
    <script type="text/javascript" src="/scripts/custom.js"></script>
    <script type="text/javascript" src="/scripts/popup-redirect.js"></script>


    <!-- Google Autocomplete -->
    <script>
        function initAutocomplete() {
            var input = document.getElementById('autocomplete-input');
            var autocomplete = new google.maps.places.Autocomplete(input);

            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }
            });

            if ($('.main-search-input-item')[0]) {
                setTimeout(function() {
                    $(".pac-container").prependTo("#autocomplete-container");
                }, 300);
            }
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initAutocomplete"></script>


    <!-- Typed Script -->
    <script type="text/javascript" src="/scripts/typed.js"></script>
    <script>
        var typed = new Typed('.typed-words', {
            strings: ["Attractions", " Restaurants", " Hotels"],
            typeSpeed: 80,
            backSpeed: 80,
            backDelay: 4000,
            startDelay: 1000,
            loop: true,
            showCursor: true
        });
    </script>


    <!-- Date Range Picker - docs: http://www.daterangepicker.com/ -->
    <script src="/scripts/moment.min.js"></script>
    <script src="/scripts/daterangepicker.js"></script>

    <script>
        $(function() {

            var start = moment().subtract(0, 'days');
            var end = moment().add(2, 'days');

            function cb(start, end) {
                $('#booking-date-search').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            cb(start, end);
            $('#booking-date-search').daterangepicker({
                "opens": "right",
                "autoUpdateInput": true,
                "alwaysShowCalendars": true,
                startDate: start,
                endDate: end
            }, cb);

            cb(start, end);

        });

        // Calendar animation and visual settings
        $('#booking-date-search').on('show.daterangepicker', function(ev, picker) {
            $('.daterangepicker').addClass('calendar-visible calendar-animated bordered-style');
            $('.daterangepicker').removeClass('calendar-hidden');
        });
        $('#booking-date-search').on('hide.daterangepicker', function(ev, picker) {
            $('.daterangepicker').removeClass('calendar-visible');
            $('.daterangepicker').addClass('calendar-hidden');
        });

        $(window).on('load', function() {
            $('#booking-date-search').val('');
        });
    </script>



</body>

</html>