<h1>Les informations de l'utilisateur sélectionné:</h1>

<textarea name="" id="" cols="30" rows="10"><?php print_r($user) ?></textarea>

<form method="post" action="">

    <?= csrf_field() ?>

    <p class="form-row form-row-wide">
        <label for="address">Adresse:
            <input type="text" class="input-text" name="address" id="address" value="<?= $address ?>"  />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="city">Ville:
            <input type="text" class="input-text" name="city" id="city" value="<?= $city ?>"  />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="postcode">Code postal:
            <input type="text" class="input-text" name="postcode" id="postcode" value="<?= $postcode ?>"  />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="country">Pays:
            <input type="text" class="input-text" name="country" id="country" value="<?= $country ?>"  />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="phone">Téléphone:
            <input type="text" class="input-text" name="phone" id="phone" value="<?= $phone ?>"  />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="facebook">Facebook:
            <input type="text" class="input-text" name="facebook" id="facebook" value="<?= $facebook ?>"  />
        </label>
    </p>
    
    <p class="form-row form-row-wide">
        <label for="twitter">Twitter:
            <input type="text" class="input-text" name="twitter" id="twitter" value="<?= $twitter ?>"  />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="linkedin">LinkedIn:
            <input type="text" class="input-text" name="linkedin" id="linkedin" value="<?= $linkedin ?>"  />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="website">Site:
            <input type="text" class="input-text" name="website" id="website" value="<?= $website ?>"  />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="logo">Logo:
            <input type="text" class="input-text" name="logo" id="logo" value="<?= $logo ?>"  />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="title">Titre:
            <input type="text" class="input-text" name="title" id="title" value="<?= $title ?>"  />
        </label>
    </p>

    <p class="form-row form-row-wide">
        <label for="description">Description:
            <textarea name="description" id="description" cols="30" rows="10"><?= $description ?></textarea>
        </label>
    </p>
    <input type="hidden" name="coach_id" value="<?= $coach_id ?>">



    <input type="hidden" name="action" value="post">

    <input type="submit" class="button border fw margin-top-10" name="register" value="Register" />

</form>