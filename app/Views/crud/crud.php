<div class="row">
    <div class="col-md-2">
        <a class="headline margin-top-70 margin-bottom-30" href="#table-user" id="back-top"><i class="fa fa-anchor"></i> table user</a>
    </div>
    <div class="col-md-2">
        <a class="headline margin-top-70 margin-bottom-30" href="#table-user-role"><i class="fa fa-anchor"></i> Table user role</a>
    </div>
    <div class="col-md-2">
        <a class="headline margin-top-70 margin-bottom-30" href="#table-coach"><i class="fa fa-anchor"></i> Table coach</a>
    </div>
    <div class="col-md-2">
        <a class="headline margin-top-70 margin-bottom-30" href="#table-category"><i class="fa fa-anchor"></i> Table catégorie</a>
    </div>

</div>

<div class="row">
    <div class="col-md-2">
        <h4 class="headline margin-top-70 margin-bottom-30" id="table-user">Table user </h4>
    </div>

    <div class="col-md-1">
        <form action="add/user" method="post">
            <?= csrf_field() ?>
            <input type="hidden" name="action" value="edit">
            <button type="submit" class="btn-add headline margin-top-70 margin-bottom-30">add</button>
        </form>
    </div>
    <div class="col-md-1">
        <button id="btn-table-user" class="btn-edit headline margin-top-70 margin-bottom-30">look</button>
    </div>

</div>


<table class="basic-table">
    <tr>
        <td>action</td>
        <td># user_id</td>
        <td>first_name</td>
        <td>last_name</td>
        <td class="show-table-user">email</td>
        <td class="show-table-user">picture</td>
        <td class="show-table-user">password</td>
    </tr>
    <?php foreach ($user as $infoUser) { ?>

        <tr>
            <td>
                <form action="edit/user" method="post">
                    <?= csrf_field() ?>
                    <input type="hidden" name="user_id" value="<?= $infoUser['user_id'] ?>">
                    <input type="hidden" name="first_name" value="<?= $infoUser['first_name'] ?>">
                    <input type="hidden" name="last_name" value="<?= $infoUser['last_name'] ?>">
                    <input type="hidden" name="email" value="<?= $infoUser['email'] ?>">
                    <input type="hidden" name="picture" value="<?= $infoUser['picture'] ?>">
                    <input type="hidden" name="password" value="<?= $infoUser['password'] ?>">
                    <input type="hidden" name="action" value="edit">
                    <button type="submit" class="btn-edit">Edit</button>
                </form>
                <form action="del/user" method="post" onsubmit="return confirm('Etes-vous sûr de vouloir supprimer définitivement utilisateur <?= $infoUser['first_name'] ?> <?= $infoUser['first_name'] ?> id <?= $infoUser['user_id'] ?>? Cette action entraînera la suppression des informations de l\'utilisateur dans table coach et table user role.');">
                    <?= csrf_field() ?>
                    <input type="hidden" name="user_id" value="<?= $infoUser['user_id'] ?>">
                    <input type="hidden" name="first_name" value="<?= $infoUser['first_name'] ?>">
                    <input type="hidden" name="last_name" value="<?= $infoUser['last_name'] ?>">
                    <button type="submit" class="btn-del">del</button>
                </form>
            </td>

            <td data-label="user_id">#<?= $infoUser['user_id'] ?></td>
            <td data-label="first_name"><?= $infoUser['first_name'] ?></td>
            <td data-label="last_name"><?= $infoUser['last_name'] ?></td>
            <td data-label="email" class="show-table-user"><?= $infoUser['email'] ?></td>
            <td data-label="picture" class="show-table-user"><img src="<?= $infoUser['picture'] ?>" alt="" height="12%"></td>
            <td data-label="password" class="show-table-user"><?= $infoUser['password'] ?></td>


        </tr>

    <?php } ?>

</table >


<div class="row">
    <div class="col-md-3">
        <h4 class="headline margin-top-70 margin-bottom-30" id="table-user-role">Table user role</h4>
    </div>

</div>
<table class="basic-table">
    <tr>
        <td>action</td>
        <td># user_role_id</td>
        <td><i class="sl sl-icon-key"></i>user_id</td>
        <td>role_id</td>

    </tr>
    <?php foreach ($userRoles as $infoUserRole) { ?>

        <tr>
            <td>
                <form action="edit/userRole" method="post">
                    <?= csrf_field() ?>
                    <input type="hidden" name="user_role_id" value="<?= $infoUserRole['user_role_id'] ?>">
                    <input type="hidden" name="user_id" value="<?= $infoUserRole['user_id'] ?>">
                    <input type="hidden" name="role_id" value="<?= $infoUserRole['role_id'] ?>">
                    <input type="hidden" name="action" value="edit">
                    <button type="submit" class="btn-edit">edit</button>
            </td>

            </form>
            <td data-label="user_role_id">#<?= $infoUserRole['user_role_id'] ?></td>
            <td data-label="user_id"><i class="sl sl-icon-key"></i><?= $infoUserRole['user_id'] ?></td>
            <td data-label="role_id"><?php
                                            switch ($infoUserRole['role_id']) {
                                                case '1':
                                                    echo "1 admin";
                                                    break;
                                                case '2':
                                                    echo "2 coach";
                                                    break;
                                                case '3':
                                                    echo "3 user";
                                                    break;
                                                default:
                                                    echo "$infoUserRole[role_id]";
                                            } ?></td>




        </tr>

    <?php } ?>

</table>



<div class="row">
    <div class="col-md-2">
        <h4 class="headline margin-top-70 margin-bottom-30" id="table-coach">Table coach</h4>
    </div>
    <div class="col-md-1">
        <button id="btn-table-coach" class="btn-edit headline margin-top-70 margin-bottom-30">look</button>
    </div>
</div>
<table class="basic-table">
    <tr>
        <td>action</td>
        <td class="show-table-coach"># coach_id</td>
        <td><i class="sl sl-icon-key"></i>user_id</td>
        <td>address</td>
        <td>city</td>
        <td class="show-table-coach">postcode</td>
        <td class="show-table-coach">country</td>
        <td class="show-table-coach">geocod_lat</td>
        <td class="show-table-coach">geocod_lng</td>
        <td class="show-table-coach">phone</td>
        <td class="show-table-coach">facebook</td>
        <td class="show-table-coach">twitter</td>
        <td class="show-table-coach">linkedin</td>
        <td class="show-table-coach">website</td>
        <td class="show-table-coach">logo</td>
        <td class="show-table-coach">title</td>
        <td class="show-table-coach">description</td>
    </tr>

    <?php foreach ($coach as $infoCoach) { ?>

        <tr>
            <td>
                <form action="edit/coach" method="post">
                    <?= csrf_field() ?>
                    <input type="hidden" name="user_id"" value=" <?= $infoCoach['user_id'] ?>">
                    <input type="hidden" name="coach_id" value="<?= $infoCoach['coach_id'] ?>">
                    <input type="hidden" name="address" value="<?= $infoCoach['address'] ?>">
                    <input type="hidden" name="city" value="<?= $infoCoach['city'] ?>">
                    <input type="hidden" name="postcode" value="<?= $infoCoach['postcode'] ?>">
                    <input type="hidden" name="country" value="<?= $infoCoach['country'] ?>">
                    <input type="hidden" name="geocod_lat" value="<?= $infoCoach['geocod_lat'] ?>">
                    <input type="hidden" name="geocod_lng" value="<?= $infoCoach['geocod_lng'] ?>">
                    <input type="hidden" name="phone" value="<?= $infoCoach['phone'] ?>">
                    <input type="hidden" name="facebook" value="<?= $infoCoach['facebook'] ?>">
                    <input type="hidden" name="twitter" value="<?= $infoCoach['twitter'] ?>">
                    <input type="hidden" name="linkedin" value="<?= $infoCoach['linkedin'] ?>">
                    <input type="hidden" name="website" value="<?= $infoCoach['website'] ?>">
                    <input type="hidden" name="logo" value="<?= $infoCoach['logo'] ?>">
                    <input type="hidden" name="title" value="<?= $infoCoach['title'] ?>">
                    <input type="hidden" name="description" value="<?= $infoCoach['description'] ?>">
                    <input type="hidden" name="action" value="edit">
                    <button type="submit" class="btn-edit">edit</button>

                </form>
            </td>
            <td data-label="coach_id" class="show-table-coach">#<?= $infoCoach['coach_id'] ?></td>
            <td data-label="user_id"><i class="sl sl-icon-key"></i><?= $infoCoach['user_id'] ?></td>
            <td data-label="address"><?= $infoCoach['address'] ?></td>
            <td data-label="city"><?= $infoCoach['city'] ?></td>
            <td data-label="postcode" class="show-table-coach"><?= $infoCoach['postcode'] ?></td>
            <td data-label="country" class="show-table-coach"><?= $infoCoach['country'] ?></td>
            <td data-label="geocod_lat" class="show-table-coach"><?= $infoCoach['geocod_lat'] ?></td>
            <td data-label="geocod_lng" class="show-table-coach"><?= $infoCoach['geocod_lng'] ?></td>
            <td data-label="phone" class="show-table-coach"><?= $infoCoach['phone'] ?></td>
            <td data-label="facebook" class="show-table-coach"><?= $infoCoach['facebook'] ?></td>
            <td data-label="twitter" class="show-table-coach"><?= $infoCoach['twitter'] ?></td>
            <td data-label="linkedin" class="show-table-coach"><?= $infoCoach['linkedin'] ?></td>
            <td data-label="website" class="show-table-coach"><?= $infoCoach['website'] ?></td>
            <td data-label="logo" class="show-table-coach"><?= $infoCoach['logo'] ?></td>
            <td data-label="title" class="show-table-coach"><?= $infoCoach['title'] ?></td>
            <td data-label="description" class="show-table-coach"><textarea class="textarea_crud" cols="num" rows="num"><?= $infoCoach['description'] ?></textarea></td>
        </tr>

    <?php } ?>

</table>


<div class="row">
    <div class="col-md-2">
        <h4 class="headline margin-top-70 margin-bottom-30" id="table-category">Table catégorie</h4>
    </div>

    <div class="col-md-1">
        <form action="add/categorie" method="post">
            <?= csrf_field() ?>
            <input type="hidden" name="action" value="edit">
            <button type="submit" class="btn-add headline margin-top-70 margin-bottom-30">add</button></form>
    </div>
</div>

<table class="basic-table">
    <tr>
        <td>action</td>
        <td># category_id</td>
        <td>name</td>
    </tr>
    <?php foreach ($categorie as $infoCat) { ?>

        <tr>
            <td>
                <form action="edit/catergorie" method="post">
                    <?= csrf_field() ?>
                    <input type="hidden" name="category_id" value="<?= $infoCat['category_id'] ?>">
                    <input type="hidden" name="name" value="<?= $infoCat['name'] ?>">
                    <input type="hidden" name="action" value="edit">
                    <button type="submit" class="btn-edit">edit</button>
                </form>

                <form action="del/catergorie" method="post" onsubmit="return confirm('Êtes-vous sûr de vouloir supprimer définitivement la catégorie <?= $infoCat['name'] ?> ? Cette action entraînera la suppression des informations de catégories dans la table CoachCategory.');">
                    <?= csrf_field() ?>
                    <input type="hidden" name="category_id" value="<?= $infoCat['category_id'] ?>">
                    <input type="hidden" name="name" value="<?= $infoCat['name'] ?>">

                    <button type="submit" class="btn-del">del</button>
                </form>

            </td>
            <td data-label="category_id">#<?= $infoCat['category_id'] ?></td>
            <td data-label="name"><?= $infoCat['name'] ?></td>
        </tr>

    <?php } ?>

</table>




<!-- $coachCategory['coach_category_id']
$coachCategory['coach_id']
$coachCategory['category_id'] -->













<div class="row">
    <div class="col-md-3">
        <h2 class="headline margin-top-70 margin-bottom-30">Vue des tables sur le CRUD</h2>
    </div>
    <div class="col-md-1">
        <button id="btn-vue" class="btn-edit headline margin-top-70 margin-bottom-30">look</button>
    </div>

</div>
<section class='vue-crud-table'>

    <div class="row">
        <div class="col-md-4">
            <h3>User</h1>
                <textarea name="" id="" cols="30" rows="10">
    <?php if (isset($user)) {
        print_r($user);
    }
    ?>
    </textarea>
        </div>
        <div class="col-md-4">
            <h3>user Roles</h1>
                <textarea name="" id="" cols="30" rows="10">
        <?php if (isset($userRoles)) {
            print_r($userRoles);
        }
        ?>
    </textarea>
        </div>
        <div class="col-md-4">
            <h3>coach</h1>
                <textarea name="" id="" cols="30" rows="10">
                <?php if (isset($coach)) {
                    print_r($coach);
                }
                ?>
                </textarea>
        </div>
        <div class="col-md-4">
            <h3>Catégorie</h1>
                <textarea name="" id="" cols="30" rows="10">
    <?php if (isset($categorie)) {
        print_r($categorie);
    }
    ?>
    </textarea>
        </div>
        <div class="col-md-4">
            <h3>Les catégories du coach: </h3>
            <textarea name="" id="" cols="30" rows="10">
    <?php if (isset($coachCategory)) {
        print_r($coachCategory);
    }
    ?>
    </textarea>
        </div>
    </div>
</section>