<h2>Créé une nouvelle catégorie</h2>

<form method="post" action="" >

    <?= csrf_field() ?>
  
    <p class="form-row form-row-wide">
        <label for="name">Nom de la catégorie:

            <input type="text" class="input-text" name="name" id="name" value="" required />
        </label>
    </p>

    <input type="hidden" name="action" value="post">

    <input type="submit" class="button border fw margin-top-10" name="register" value="Register" />

</form>