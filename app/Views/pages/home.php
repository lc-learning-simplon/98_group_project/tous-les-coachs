
<!-- Banner
================================================== -->
<div class="main-search-container  alt-search-box centered" style="background-image:url('images/mountain.jpg');  ">
	<div class="main-search-inner">

		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<div class="main-search-input">

						<div class="main-search-input-headline">
							<h2>Bienvenue sur la plateforme qui vous aide à atteindre vos objectifs.</h2>
							<h4>Trouvez des professionnels, des livres mais aussi des formations pour vous accompagner dans vos projets.</h4>
						</div>

						<div class="main-search-input-item location">
							<div id="autocomplete-container">
								<input id="autocomplete-input" type="text" placeholder="Location">
							</div>
							<a href="#"><i class="fa fa-map-marker"></i></a>
						</div>

	

						<div class="main-search-input-item">
							<select data-placeholder="All Categories" class="chosen-select" >
								<option>All Categories</option>	
								<option>Shops</option>
								<option>Hotels</option>
								<option>Restaurants</option>
								<option>Fitness</option>
								<option>Events</option>
							</select>
						</div>

						<button class="button" onclick="window.location.href='listings-half-screen-map-list.html'">Rechercher</button>

					</div>
				</div>
			</div>
			
		</div>

	</div>
</div>





<!-- Content
================================================== -->

<!-- Info Section -->
<div class="container">

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2 class="headline centered margin-top-80">
				  Lancez-vous!
				<span class="margin-top-25">Quelque soit votre projet, nous vous rapprochons des ressources les plus aptes à vous aider.</span>
			</h2>
		</div>
	</div>

	<div class="row icons-container">
		<!-- Stage -->
		<div class="col-md-4">
			<div class="icon-box-2 with-line">
				<i class=" im im-icon-Affiliate"></i>
				<h3>Mise en relation avec un coach</h3>
				<p>Proin dapibus nisl ornare diam varius tempus. Aenean a quam luctus, finibus tellus ut, convallis eros sollicitudin turpis.</p>
				<a style="color:#f91942 " href='/coach'> + Retrouvez tous les coachs près de chez vous. </a>
			</div>
		</div>

		<!-- Stage -->
		<div class="col-md-4">
			<div class="icon-box-2 with-line">
				<i class="im im-icon-Book"></i>
				<h3>Lectures recommandées</h3>
				<p>Maecenas pulvinar, risus in facilisis dignissim, quam nisi hendrerit nulla, id vestibulum metus nullam viverra porta purus.</p>
				<a style="color:#f91942 " href='/livre'> + Faites le plein de connaissances.</a>

			</div>
		</div>

		<!-- Stage -->
		<div class="col-md-4">
			<div class="icon-box-2">
				<i class="im  im-icon-Student-Male"></i>
				<h3>Offre des organismes de formation</h3>
				<p>Faucibus ante, in porttitor tellus blandit et. Phasellus tincidunt metus lectus sollicitudin feugiat pharetra consectetur.</p>
				<a style="color:#f91942 " href='/formation' > + Découvrez toutes les formations alentours.</a>

			</div>
		</div>
	</div>

</div>
<!-- Info Section / End -->

<!-- Flip banner -->
<div class="flip-banner parallax" data-background="images/slider-bg-02.jpg" data-color="#f91942" data-color-opacity="0.85" data-img-width="2500" data-img-height="1666">

</div>
<!-- Flip banner / End -->