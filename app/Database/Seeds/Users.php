<?php

namespace App\Database\Seeds;

class Users extends \CodeIgniter\Database\Seeder
{
    public function run()
    {

        /**
         * Seed admin pour les test, à modifier avant le passage en production.
         */

        $data = [
            'first_name'    => 'Admin',
            'last_name'     => 'Admin',
            'email'         => 'admin@admin.com',
            'password'      => hash('sha384',"admin")
        ];
        $this->db->table('users')->insert($data);

        $data = [
            'user_id'   => 1,
            'role_id'   => 1
        ];
        $this->db->table('user_role')->insert($data);

        /**
         * Seed coach pour les test, à modifier avant le passage en production.
         */

        $data = [
            'first_name'    => 'Coach',
            'last_name'     => 'Coach',
            'email'         => 'coach@coach.com',
            'password'      => hash('sha384',"coach")
        ];
        $this->db->table('users')->insert($data);

        $data = [
            'user_id'   => 2,
            'role_id'   => 2
        ];
        $this->db->table('user_role')->insert($data);

        $data = [
            'user_id'   => 2,
            'address'    => '231 Rue Pierre et Marie Curie',
            'city'      =>  'Labège',
            'postcode'  =>  31670,
            'country'   => 'France',
            'geocod_lat'=>  43.545881,
            'geocod_lng'=>  1.513863,
            'phone'     =>  "0543786475",
            'title'     =>  "Salut la compagnie !",
            'description'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ];
        $this->db->table('coachs')->insert($data);

        /**
         * Seed user pour les test, à modifier avant le passage en production.
         */

        $data = [
            'first_name'    => 'User',
            'last_name'     => 'User',
            'email'         => 'user@user.com',
            'password'      => hash('sha384',"user")
        ];
        $this->db->table('users')->insert($data);

        $data = [
            'user_id'   => 3,
            'role_id'   => 3
        ];
        $this->db->table('user_role')->insert($data);
        
    }
}
