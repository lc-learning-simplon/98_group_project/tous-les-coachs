<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Coach extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();
		$this->forge->addField([
			'coach_id'		=> [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'user_id'		=> [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE
			],
			'address'		=> [
				'type'			 => 'VARCHAR',
				'constraint'	 => '100',
				'default'	 	 => "NA",
			],
			'city'		=> [
				'type'			 => 'VARCHAR',
				'constraint'	 => '100',
				'default'	 	 => "NA",
			],
			'postcode'		=> [
				'type'			 => 'VARCHAR',
				'constraint'	 => '5',
				'default'	 	 => "NA",
			],
			'country'		=> [
				'type'			 => 'VARCHAR',
				'constraint'	 => '100',
				'default'	 	 => "NA",
			],
			'geocod_lat'		=> [
				'type'			 => 'VARCHAR',
				'constraint'	 => '100',
				'default'	 	 => "NA",
			],
			'geocod_lng'		=> [
				'type'			 => 'VARCHAR',
				'constraint'	 => '100',
				'default'	 	 => "NA",
			],
			'phone' => [
				'type'           => 'VARCHAR',
				'constraint'	 => '100',
				'default'	 	 => "NA",
			],
			'facebook'		=> [
				'type'			 => 'VARCHAR',
				'constraint'	 => '100',
				'null'			 => TRUE,
				'default'	 	 => "",
			],
			'twitter'		=> [
				'type'			 => 'VARCHAR',
				'constraint'	 => '100',
				'null'			 => TRUE,
				'default'	 	 => "",
			],
			'linkedin'		=> [
				'type'			 => 'VARCHAR',
				'constraint'	 => '100',
				'null'			 => TRUE,
				'default'	 	 => "",
			],
			'website'		=> [
				'type'			 => 'VARCHAR',
				'constraint'	 => '100',
				'null'			 => TRUE,
				'default'	 	 => "",
			],
			'logo'		=> [
				'type'			 => 'TEXT',
				'null'			 => TRUE,
				'default'	 	 => "",
			],
			'title'			=> [
				'type'			 => 'VARCHAR',
				'constraint'	 => '100',
				'null'			 => TRUE,
			],
			'description' 	=> [
                'type'           => 'TEXT',
				'null'           => TRUE,
			],
			'rating' 	=> [
				'type'           => 'TINYINT',
				'contraint'		 => 4,
				'default'		 => 0,
			],
			'show_rating' 	=> [
                'type'           => 'TINYINT',
				'constraint'	 => 1,
				'default'	 	 => 0,
			],
			'validate'		=> [
				'type'			 => 'TINYINT',
				'constraint'	 => 1,
				'default'	 	 => 0,
			]
		]);
		$this->forge->addForeignKey('user_id', 'users', 'user_id');
		$this->forge->addKey('coach_id', TRUE);
		$this->forge->createTable('coachs');
		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('coachs');
	}
}
