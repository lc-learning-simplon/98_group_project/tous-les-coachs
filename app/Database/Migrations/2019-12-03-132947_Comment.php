<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Comment extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();
		$this->forge->addField([
			'comment_id' =>	[
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'user_id' =>	[
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE
			],
			'content'	=>	[
				'type' =>	'TEXT'
			]
		]);
		$this->forge->addForeignKey('user_id', 'users', 'user_id');
		$this->forge->addKey('comment_id', TRUE);
		$this->forge->createTable('comments');
		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('comments');
	}
}
