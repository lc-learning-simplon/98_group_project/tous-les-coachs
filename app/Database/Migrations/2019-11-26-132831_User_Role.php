<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserRole extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();
		$this->forge->addField([
			'user_role_id'		=> [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'user_id'		=> [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE
			],
			'role_id'		=> [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE
			]
		]);
		$this->forge->addForeignKey('user_id', 'users', 'user_id');
		$this->forge->addForeignKey('role_id', 'roles', 'role_id');
		$this->forge->addKey('user_role_id', TRUE);
		$this->forge->createTable('user_role');
		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('user_role');
	}
}
