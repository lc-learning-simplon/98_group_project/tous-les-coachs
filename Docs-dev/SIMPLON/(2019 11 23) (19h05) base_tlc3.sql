-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  sam. 23 nov. 2019 à 18:06
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `base_tlc3`
--

-- --------------------------------------------------------

--
-- Structure de la table `autre_categorie_coaching`
--

DROP TABLE IF EXISTS `autre_categorie_coaching`;
CREATE TABLE IF NOT EXISTS `autre_categorie_coaching` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `nom` int(11) NOT NULL COMMENT 'nom de la catégorie de coaching proposée',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='pour que les coachs puissent proposer de nouvelles catégorie';

-- --------------------------------------------------------

--
-- Structure de la table `categorie_coaching`
--

DROP TABLE IF EXISTS `categorie_coaching`;
CREATE TABLE IF NOT EXISTS `categorie_coaching` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID de la catégorie de coaching',
  `nom` varchar(30) NOT NULL COMMENT 'nom de la catégorie',
  `popularité` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'popularité de cette catégorie auprès des visiteurs',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Ce sont les différents catégories de coaching';

--
-- Déchargement des données de la table `categorie_coaching`
--

INSERT INTO `categorie_coaching` (`ID`, `nom`, `popularité`) VALUES
(1, 'Autre', 0),
(2, 'Développement personnel', 0),
(3, 'Santé', 0),
(4, 'Sport', 0),
(5, 'Business', 0),
(6, 'Psychologie', 0),
(7, 'cat5', 0),
(8, 'cat6', 0),
(9, 'cat7', 0),
(10, 'cat8', 0),
(11, 'cat9', 0);

-- --------------------------------------------------------

--
-- Structure de la table `coach`
--

DROP TABLE IF EXISTS `coach`;
CREATE TABLE IF NOT EXISTS `coach` (
  `coach_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID du coach',
  `coach_prenom` varchar(50) NOT NULL COMMENT 'Prenom du coach',
  `coach_nom` varchar(50) NOT NULL COMMENT 'Nom du coach',
  `coach_title` varchar(30) NOT NULL COMMENT 'titre du coach',
  `coach_comment_3lines` varchar(150) NOT NULL COMMENT 'commentaire libre pour le coach, en 3 lignes max',
  `coach_adr` varchar(50) NOT NULL COMMENT 'adresse du coach',
  `coach_adr_pays` varchar(30) NOT NULL COMMENT 'adresse du coach : nom du pays',
  `coach_geocod_lat` decimal(11,8) NOT NULL COMMENT 'la latitude du coach',
  `coach_geocod_lng` decimal(11,8) NOT NULL COMMENT 'la longitude du coach',
  `coach_tel` varchar(15) NOT NULL COMMENT 'nupméro de télephone',
  `coach_email` varchar(30) NOT NULL COMMENT 'email du coach',
  `coach_image` varchar(256) NOT NULL COMMENT 'adresse du fichier image avec la photo du coach',
  `coach_logo` varchar(256) NOT NULL COMMENT 'adresse du fichier avec le logo du coach',
  `coach_page_web_perso` varchar(100) NOT NULL COMMENT 'page web perso du coach',
  `coach_note` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'note moyenne donnée au coach par ses clients',
  `coach_option_aff_note` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'option permettant au coach de choisir s''il veut que sa note soit affichée ou non',
  `coach_test` tinyint(1) NOT NULL COMMENT 'Vrai pour les coachs fictifs juste là pour faire des tests',
  `coach_valide` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'true si le coach a été validé',
  `coach_nouvelle_modif` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 si nouvelles modifications depuis la validation',
  PRIMARY KEY (`coach_id`),
  KEY `coach_geocod_lat` (`coach_geocod_lat`),
  KEY `coach_geocod_lng` (`coach_geocod_lng`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `coach`
--

INSERT INTO `coach` (`coach_id`, `coach_prenom`, `coach_nom`, `coach_title`, `coach_comment_3lines`, `coach_adr`, `coach_adr_pays`, `coach_geocod_lat`, `coach_geocod_lng`, `coach_tel`, `coach_email`, `coach_image`, `coach_logo`, `coach_page_web_perso`, `coach_note`, `coach_option_aff_note`, `coach_test`, `coach_valide`, `coach_nouvelle_modif`) VALUES
(1, 'prenom 1', 'nom 1', 'titre coach 1', 'Un coach bienveillant et à votre écoute.\r\nDe nombreuses années d\'expérience.', '1 rue un, 11111 ville1', 'France', '43.57253500', '1.49700700', '1111111111', 'coach1@email.com', '', '', '', 0, 1, 1, 1, 0),
(2, 'prenom 2', 'nom 2', 'titre coach 2', 'Pour devenir totalement autonomes sur votre santé !', '2 rue Deux, 22222 Ville2', 'France', '43.60848400', '1.49139600', '2222222222', 'mail2@email.com', '', '', 'www.coach2.free.fr', 1, 1, 1, 1, 0),
(3, 'prenom3', 'nom3', 'titre coach 3', 'Je m\'engage à booster vos performances.\r\nCoaching individuel ou coaching d\'équipe.', '3 rue Trois, 33333 Ville3', 'France', '43.64577200', '1.47637300', '3333333333', 'mail3@email.com', '', '', '', 2, 1, 1, 1, 0),
(4, 'prenom4', 'nom4', 'titre coach 4', 'Je vous accompagne dans toutes les étapes pour monter votre entreprise.', '4 rue Quatre, 44444 Ville4', 'France', '43.65076400', '1.44089000', '4444444444', 'mail4@email.com', '', '', '', 3, 0, 1, 1, 0),
(5, 'prenom5', 'nom5', 'titre coach 5', 'différentes thérapies : hypnose, ...', '5 rue Cinq, 55555 Ville5', 'France', '43.64056400', '1.41705900', '5555555555', 'mail5@email.com', '', '', '', 4, 0, 1, 0, 0),
(6, 'prenom6', 'nom6', 'titre coach 6', '', '6 rue Six, 66666 Ville6', 'France', '43.61817500', '1.41201600', '6666666666', 'mail6@email.com', '', '', '', 5, 1, 1, 1, 0),
(7, 'prenom7', 'nom7', 'titre coach 7', '', '7 rue Sept, 77777 Ville7', 'France', '43.59606800', '1.39388000', '7777777777', 'mail7@email.com', '', '', '', 6, 1, 1, 1, 0),
(8, 'prenom8', 'nom8', 'titre coach 8', '', '8 rue Huit, 88888 Ville8', 'France', '43.57681800', '1.40366400', '8888888888', 'mail8@email.com', '', '', '', 7, 1, 1, 1, 0),
(9, 'prenom9', 'nom9', 'titre coach 9', '', '9 rue Neuf, 99999 Ville9', 'France', '43.56654500', '1.42376000', '9999999999', 'mail9@email.com', '', '', '', 8, 1, 1, 1, 0),
(10, 'prenom10', 'nom10', 'titre coach 10', '', '10 rue Dix, 10010 Ville10', 'France', '43.56433000', '1.48066600', '1010101010', 'mail10@email.com', '', '', '', 9, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `creneau`
--

DROP TABLE IF EXISTS `creneau`;
CREATE TABLE IF NOT EXISTS `creneau` (
  `cr_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'numéro id du champ',
  `coach_id` int(11) NOT NULL COMMENT 'numéro id du coach dont c''est un créneau',
  `cr_jour` enum('lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche') NOT NULL DEFAULT 'lundi' COMMENT 'Jour de la semaine pour lequel est défini le créneau',
  `cr_heure_deb` tinyint(3) UNSIGNED NOT NULL COMMENT 'heure de début du créneau',
  `cr_minutes_deb` tinyint(3) UNSIGNED NOT NULL COMMENT 'minutes du début du créneau',
  `cr_heure_fin` tinyint(3) UNSIGNED NOT NULL COMMENT 'heure de fin du créneau',
  `cr_minutes_fin` tinyint(3) UNSIGNED NOT NULL COMMENT 'minutes de la fin du créneau',
  PRIMARY KEY (`cr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `creneau`
--

INSERT INTO `creneau` (`cr_id`, `coach_id`, `cr_jour`, `cr_heure_deb`, `cr_minutes_deb`, `cr_heure_fin`, `cr_minutes_fin`) VALUES
(1, 1, 'lundi', 8, 1, 9, 2),
(2, 2, 'mardi', 9, 0, 10, 0),
(3, 2, 'mercredi', 10, 30, 11, 30),
(4, 3, 'jeudi', 12, 15, 13, 15),
(5, 4, 'vendredi', 14, 30, 16, 30),
(6, 5, 'samedi', 16, 0, 18, 30),
(7, 6, 'dimanche', 18, 0, 19, 0),
(8, 7, 'lundi', 8, 0, 12, 0),
(9, 8, 'mardi', 8, 0, 18, 0),
(10, 9, 'mercredi', 12, 0, 18, 0),
(11, 10, 'jeudi', 15, 0, 21, 0),
(12, 11, 'lundi', 12, 0, 14, 0);

-- --------------------------------------------------------

--
-- Structure de la table `offre_coach`
--

DROP TABLE IF EXISTS `offre_coach`;
CREATE TABLE IF NOT EXISTS `offre_coach` (
  `ofc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID de l''offre',
  `coach_id` int(10) UNSIGNED NOT NULL COMMENT 'ID du coach auquel appartient l''offre',
  `ofc_cat` tinyint(3) UNSIGNED NOT NULL COMMENT 'catégorie de coaching de l''offre',
  `ofc_duree_seance` decimal(4,2) UNSIGNED NOT NULL COMMENT 'durée d''une séance de coaching',
  `ofc_prix` decimal(6,2) UNSIGNED NOT NULL COMMENT 'prix d''une séance de coaching',
  `ofc_devise` enum('€','Francs suisses','$ canadiens','Francs CFA') NOT NULL DEFAULT '€' COMMENT 'devise dans laquelle est exprimé le prix',
  `ofc_gratuite_seance1` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Gratuité de la 1ère séance',
  PRIMARY KEY (`ofc_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='tables des différentes offres proposées par les coachs';

--
-- Déchargement des données de la table `offre_coach`
--

INSERT INTO `offre_coach` (`ofc_id`, `coach_id`, `ofc_cat`, `ofc_duree_seance`, `ofc_prix`, `ofc_devise`, `ofc_gratuite_seance1`) VALUES
(1, 1, 0, '1.00', '125.00', '€', 1),
(2, 2, 1, '2.00', '90.00', '€', 0),
(3, 3, 2, '1.50', '75.00', '€', 0),
(4, 4, 3, '2.00', '35.00', '€', 0),
(5, 5, 4, '1.00', '0.00', '€', 1),
(6, 6, 5, '1.50', '300.00', '€', 1),
(7, 7, 6, '2.00', '130.00', '€', 0),
(8, 8, 7, '2.00', '80.00', '€', 0),
(9, 9, 8, '2.50', '200.00', '€', 0),
(10, 10, 9, '2.00', '0.00', '€', 1),
(11, 1, 2, '1.50', '225.00', '€', 1);

-- --------------------------------------------------------

--
-- Structure de la table `statclic`
--

DROP TABLE IF EXISTS `statclic`;
CREATE TABLE IF NOT EXISTS `statclic` (
  `sc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'n° de la stat',
  `sc_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'timestamp ajouté automatiquement',
  `sc_nomPage` varchar(20) NOT NULL COMMENT 'nom de la page qui a été cliquée',
  `sc_versionPage` tinyint(3) UNSIGNED NOT NULL COMMENT 'version de la page qui a été cliquée',
  `sc_largeurEcran` int(10) UNSIGNED NOT NULL COMMENT 'largeur de l''écran de l''utilisateur',
  `sc_hauteurEcran` int(10) UNSIGNED NOT NULL COMMENT 'hauteur de l''écran de l''utilisateur',
  `sc_nom` varchar(40) NOT NULL COMMENT 'nom de l''élément cliqué',
  `sc_param1` varchar(40) NOT NULL COMMENT 'paramètre 1 de l''élément cliqué',
  `sc_param2` varchar(40) NOT NULL COMMENT 'param2 de l''élément cliqué',
  `sc_lienSortie` tinyint(1) NOT NULL COMMENT 'true si l''élément cliqué est un lien de sortie de la page',
  `sc_priorite` tinyint(3) UNSIGNED NOT NULL COMMENT 'importance de la stat (1 = priorité la plus haute)',
  `sc_numClic` tinyint(3) UNSIGNED NOT NULL COMMENT 'numéro du clic utilisateur, c''est à dire le nombre de clic qu''il a fait sur la page avant de cliquer sur cet élément.',
  PRIMARY KEY (`sc_id`)
) ENGINE=MyISAM AUTO_INCREMENT=175 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `statclic`
--

INSERT INTO `statclic` (`sc_id`, `sc_date`, `sc_nomPage`, `sc_versionPage`, `sc_largeurEcran`, `sc_hauteurEcran`, `sc_nom`, `sc_param1`, `sc_param2`, `sc_lienSortie`, `sc_priorite`, `sc_numClic`) VALUES
(172, '2019-11-18 20:56:39', 'pageCoachs', 1, 1519, 444, 'bouton-choix', 'Coachs', '.', 0, 2, 1),
(170, '2019-11-18 18:25:59', 'pageCoachs', 1, 1519, 444, 'bouton-choix', 'Livres', '.', 0, 2, 1),
(171, '2019-11-18 18:26:37', 'pageCoachs', 1, 1519, 444, 'bouton-choix', 'Livres', '.', 0, 2, 1),
(169, '2019-11-18 18:19:15', 'pageCoachs', 1, 1519, 444, 'bouton-choix', 'Livres', '.', 0, 2, 1),
(168, '2019-11-18 18:04:53', 'pageCoachs', 1, 1519, 444, 'bouton-choix', 'Livres', '.', 0, 2, 1),
(167, '2019-11-17 22:22:41', 'pageCoachs', 1, 1519, 444, 'bouton-choix', 'Formations', '.', 0, 2, 1),
(166, '2019-11-17 22:18:35', 'homeCoachs', 1, 1519, 740, 'bouton-choix', 'Livres', '.', 0, 2, 1),
(173, '2019-11-23 17:40:31', 'pageTest', 1, 1536, 740, 'bouton-choix', 'Livres', '.', 0, 2, 1),
(174, '2019-11-23 17:40:53', 'pageTest', 1, 1536, 444, 'bouton-choix', 'Evènements', '.', 0, 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `statpos`
--

DROP TABLE IF EXISTS `statpos`;
CREATE TABLE IF NOT EXISTS `statpos` (
  `sp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id de la stat',
  `sp_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date de la stat',
  `sp_lat` float NOT NULL COMMENT 'latitude de la position',
  `sp_lng` float NOT NULL COMMENT 'longitude de la position',
  `sp_typeLoc` tinyint(1) NOT NULL COMMENT '0 = undefined; 1 = loc forcée par page; 2 = Navigateur; 3 = google; 4 = IPAPI; 5 = user search; 6 = No Loc <=> loc par défaut',
  `sp_appareilNomade` tinyint(1) NOT NULL COMMENT 'true si l''appareil est nomale',
  `sp_nbreCoachsProxy` int(10) UNSIGNED NOT NULL COMMENT 'le nombre de coachs détectés à proximité de l''utilisateur',
  PRIMARY KEY (`sp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='positions utilisateurs, ou recherchées par les utilisateurs';

--
-- Déchargement des données de la table `statpos`
--

INSERT INTO `statpos` (`sp_id`, `sp_date`, `sp_lat`, `sp_lng`, `sp_typeLoc`, `sp_appareilNomade`, `sp_nbreCoachsProxy`) VALUES
(1, '2019-10-09 16:17:17', 43.4827, 1.4306, 4, 1, 9),
(2, '2019-10-12 15:30:21', 43.4827, 1.4306, 4, 1, 9),
(3, '2019-10-12 16:28:10', 43.4827, 1.4306, 4, 1, 9),
(4, '2019-10-12 16:29:20', 43.4827, 1.4306, 4, 1, 9);

-- --------------------------------------------------------

--
-- Structure de la table `statuser`
--

DROP TABLE IF EXISTS `statuser`;
CREATE TABLE IF NOT EXISTS `statuser` (
  `su_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `su_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date et heure de la stat',
  `su_nomPage` varchar(20) NOT NULL COMMENT 'Nom de la page à l''origine de la stat',
  `su_versionPage` tinyint(3) UNSIGNED NOT NULL COMMENT 'version de la page à l''origine de la stat',
  `su_nav` varchar(10) NOT NULL COMMENT 'type de navigateur utilisé',
  `su_largeurEcran` smallint(5) UNSIGNED NOT NULL COMMENT 'largeur de l''écran de l''utilisateur',
  `su_hauteurEcran` smallint(5) UNSIGNED NOT NULL COMMENT 'longueur de l''écran de l''utilisateur',
  `su_referrer` varchar(50) NOT NULL COMMENT 'nom de la page web d''où vient l''utilisateur',
  PRIMARY KEY (`su_id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1 COMMENT='table des statistiques utilisateurs';

--
-- Déchargement des données de la table `statuser`
--

INSERT INTO `statuser` (`su_id`, `su_date`, `su_nomPage`, `su_versionPage`, `su_nav`, `su_largeurEcran`, `su_hauteurEcran`, `su_referrer`) VALUES
(23, '2019-10-12 16:29:20', 'homeCoachs', 1, 'Firefox', 1519, 400, '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
